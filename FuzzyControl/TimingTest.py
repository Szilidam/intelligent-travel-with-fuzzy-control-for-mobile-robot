#! /usr/bin/env python

import time
import FuzzyRulebook

ExecutionTime=0
StartTime=0
FinishTime=0
iterations=10000
result=""

safetyRule=FuzzyRulebook.fuzzyruleFromFile("SafetySet.txt")
speedRule=FuzzyRulebook.fuzzyruleFromFile("MotorSpeedSet.txt")
decisionOptions=FuzzyRulebook.fuzzyruleFromFile("ActionSet.txt")
lookupTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionLookupTable.txt")

StartTime=time.time()
for i in range(0,iterations):
    FuzzyRulebook.controlAction_FIVE(safetyRule,40,speedRule,0.2,decisionOptions,lookupTable,-1)
FinishTime=time.time()
ExecutionTime=(FinishTime-StartTime)/iterations
result="FIVE: "+str(ExecutionTime)
print(result)

safetyRule=FuzzyRulebook.fuzzyruleFromFile("SafetySet_FirstExp.txt")
speedRule=FuzzyRulebook.fuzzyruleFromFile("MotorSpeedSet_FirstExp.txt")
decisionOptions=FuzzyRulebook.fuzzyruleFromFile("ActionSet.txt")
lookupTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionTable_FirstExp.txt")

StartTime=time.time()
for i in range(0,iterations):
    FuzzyRulebook.controlAction(lookupTable,safetyRule,40,speedRule,0.2)
FinishTime=time.time()
ExecutionTime=(FinishTime-StartTime)/iterations
result="Sugeno: "+str(ExecutionTime)
print(result)

safetyRule=FuzzyRulebook.fuzzyruleFromFile("SafetySet_V2.txt")
speedRule=FuzzyRulebook.fuzzyruleFromFile("MotorSpeedSet_V2.txt")
decisionOptions=FuzzyRulebook.fuzzyruleFromFile("ActionSet_Test.txt")
lookupTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionLookupTable_Test.txt")

StartTime=time.time()
for i in range(0,iterations):
    FuzzyRulebook.controlAction_V2(safetyRule,40,speedRule,0.2,decisionOptions)
FinishTime=time.time()
ExecutionTime=(FinishTime-StartTime)/iterations
result="Mamdani: "+str(ExecutionTime)
print(result)

safetyRule=FuzzyRulebook.fuzzyruleFromFile("SafetySet_Test.txt")
speedRule=FuzzyRulebook.fuzzyruleFromFile("MotorSpeedSet_Test.txt")
decisionOptions=FuzzyRulebook.fuzzyruleFromFile("ActionSet_Test.txt")
lookupTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionLookupTable_Test.txt")

StartTime=time.time()
for i in range(0,iterations):
    FuzzyRulebook.controlAction_V3(safetyRule,40,speedRule,0.2,decisionOptions,lookupTable)
FinishTime=time.time()
ExecutionTime=(FinishTime-StartTime)/iterations
result="Mamdani_table: "+str(ExecutionTime)
print(result)