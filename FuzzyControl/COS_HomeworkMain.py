#! /usr/bin/env python

import rospy
import math
import FuzzyRulebook
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan

def lidarCallback(msg):
    global laserDataStore
    laserDataStore=msg


rospy.init_node('BurgerController')
laserDataStore=LaserScan()
rate=rospy.Rate(4)
stateIndicator="initialize"
cycleCounter=0
pub=rospy.Publisher("/cmd_vel", Twist, queue_size=1)
motorDataStore=Twist()
while stateIndicator!="end":
    if stateIndicator=="initialize":
        #fuzzyControlTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionTable.txt")
        safetyRule=FuzzyRulebook.fuzzyruleFromFile("SafetySet.txt")
        speedRule=FuzzyRulebook.fuzzyruleFromFile("MotorSpeedSet.txt")
        decisionOptions=FuzzyRulebook.fuzzyruleFromFile("ActionSet.txt")
        lookupTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionLookupTable.txt")
        motorSpeed=0
        stateIndicator="aquisition"
    elif stateIndicator=="aquisition":
        rospy.Subscriber("/scan", LaserScan, lidarCallback)
        rate.sleep()
        safety=FuzzyRulebook.fuzzyfyLidarData(laserDataStore,[[0,90],[270,360]])
        decision=FuzzyRulebook.controlAction_V2(safetyRule,safety,speedRule,motorSpeed,decisionOptions)
        #decision=FuzzyRulebook.controlAction_V3(safetyRule,safety,speedRule,motorSpeed,decisionOptions,lookupTable)
        stateIndicator="motion"
    elif stateIndicator=="motion":
        motorSpeed=motorSpeed+decision
        print(motorSpeed)
        if motorSpeed>0:
            motorDataStore.linear.x=motorSpeed
            motorDataStore.angular.z=0
        else:
            motorSpeed=0
            motorDataStore.linear.x=motorSpeed
            motorDataStore.angular.z=safety*0.01+0.2
        pub.publish(motorDataStore)
        stateIndicator="looptime"
    elif stateIndicator=="looptime":
        cycleCounter+=1
        stateIndicator="aquisition"
        if cycleCounter>=100:
            stateIndicator="end"
        print(cycleCounter)
        rate.sleep()
    else:
        print("ERROR: Unknown state!")
        stateIndicator="end"

motorDataStore.linear.x=0
motorDataStore.angular.z=0
pub.publish(motorDataStore)





