#! /usr/bin/env python

import random
import math
import time
import numpy
import FuzzyRulebook
from sensor_msgs.msg import LaserScan

def empty():
    print("") #This function does nothing. But it can be given as local search function for the optimization to show that non memetic bacterial optimization is needed

def basicCostFunction(bacterium):
    summa=0
    #this is a basic costfunction. The higher the sum of chromosomes is, the fitter the bacterium is
    for i in bacterium:
        summa+=i
    return summa

def copyList(listToCopy):
    #sadly python2 does not have builtin function for copying a list, so I try to write one
    oldList=listToCopy
    newList=[]
    for i in range(0,len(oldList)):
        newList.append(oldList[i])
    return newList

class BacterialMemeticOptimization():
    def __init__(self,geneMinimum=0,geneMaximum=100,enableDifferentLength=False,costfunction=basicCostFunction,localsearchfunction=empty,initialpopulation=[]):
        self.population=initialpopulation
        self.domain=[geneMinimum,geneMaximum]
        self.diffLenEna=enableDifferentLength
        self.costFunction=costfunction
        self.localSearchFunction=localsearchfunction #If we leave this on default, there will be no local search, and the algorithm will be normal bacterial algorithm, and will not be memetic
    def InitialPopulation(self,individualSize,populationSize):
        size=individualSize
        if individualSize<=0:
            size=random.randint(6,20)
        popSize=populationSize
        if populationSize<=0:
            popSize=random.randint(3,10)
        population=[]
        for i in range(0,popSize):
            bacterium=[]
            iterCount=size #all bacteria will have the same size, even if they can be different sized at pathplanning
            for j in range(0,iterCount):
                bacterium.append(random.randint(self.domain[0],self.domain[1])) #bacteria get random integers for start and yes, in this case the algorithm is only aplicable with integers
            population.append(bacterium)
        self.population=population #so the population is represented by a two dimension array and the individuals are one dimension arrays
    def Mutation(self,numberOfClones):
        for i in range(0,len(self.population)):
            indiv= self.population[i] #for now all bacteria is mutated in this certain order
            clones=[]
            for j in range(0,numberOfClones):
                clones.append(copyList(indiv)) #we must create as many clones as it is given as parameter
            for j in range(0,len(indiv)):
                for k in range(1,len(clones)):
                    clones[k][j]=random.randint(self.domain[0],self.domain[1]) #Open point: Maybe it would be better if we would pick a random number close to the current one, but we will try it out soon
                    #Nevertheless we change one segment. We change the segments in the given order for now
                minCost=1000000
                minCostIndex=(-1)
                for k in range(0,len(clones)):
                    cost=self.costFunction(clones[k])
                    if cost<minCost:
                        minCost=cost
                        minCostIndex=k
                        #We need to find the minimum cost. This means that we always try to minimize.
                if minCostIndex>=0:
                    for clone in clones:
                        clone[j]=clones[minCostIndex][j]
                        #The fittest clone transfers its new gene to all the other clones
                        #Except if the minimum cost is 1000000. Then they are all bad as hell
            if self.diffLenEna==True:
                for k in range(1,len(clones)):
                    clones[k].append(random.randint(self.domain[0],self.domain[1])) #If there can be different sized clones, we try to add one new segment with random number
                minCost=1000000
                minCostIndex=(-1)
                for k in range(0,len(clones)):
                    cost=self.costFunction(clones[k])
                    if cost<minCost:
                        minCost=cost
                        minCostIndex=k
                        #We need to find the minimum cost. This means that we always try to minimize.
                if minCostIndex>0:
                    clones[0].append(self.domain[1]+1) #we add an impossible element to the 0th clone
                    for clone in clones:
                        clone[len(clone)-1]=clones[minCostIndex][len(clone)-1] #the last element is transferred
                    if clones[0][len(clones[0])-1]==(self.domain[1]+1):
                        clones[0].pop(len(clones[0])-1)
            print("Done with this individual")
            self.population[i]=clones[0] #We must take the 0. element. If there can be no different length, it does not matter, but if there can be, but it did not go well, when we tried to add new element, only the 0 remained untouched, and if that was the best, we take that
    def GeneTransfer(self,numberOfInfections,l_gt=1):
        superior=[]
        inferior=[]
        superiorCosts=[]
        inferiorCosts=[]
        costs=[]
        for i in self.population:
            costs.append(self.costFunction(i))
        temp=self.population
        while len(inferior)<len(temp):
            transfer=costs.index(max(costs)) #get the elements index with highest cost (least fit)
            inferior.append(temp[transfer]) #the high cost element goes to the inferior half
            inferiorCosts.append(costs[transfer]) #we also gather the costs for later use. Same costs belong to bacteria with same indexes
            temp.pop(transfer) #remove the element from temp
            costs.pop(transfer) #remove the cost
            #repeat until the inferior half becomes half as big as the population. The remaining elements will go to the superior half
        superior=temp
        superiorCosts=costs
        for i in range(0,numberOfInfections):
            sourceindex=random.randint(0,(len(superior)-1))
            destinationindex=random.randint(0,(len(inferior)-1))
            source=superior[sourceindex]
            destination=inferior[destinationindex]
            if l_gt>len(source) or l_gt>len(destination):
                l_gt=min(len(source),len(destination))
            segmentStart=random.randint(0,(len(source)-l_gt))
            #we choose randomly a source and destination bacterium, and a part or segment of the source will be transferred to the destination overwiring its genes
            for j in range(segmentStart,(segmentStart+l_gt)):
                try:
                    destination[j]=source[j] #since there is high chance that a specific element has same properties at same indexes, same properties will be modified
                except:
                    destination.append(source[j]) #if there is no place for the element in the list (can happen, when they have variable length), the source gene is added to the destination
            #superior[sourceindex]=source #actually the source bacterium is not changing, no need to overwrite it
            inferior[destinationindex]=destination #the modified destination bacterium overwrites its former self in the inferior half
            #question/open point: what if the destination will become good enough to get to the superior half
            cost=self.costFunction(destination)
            if cost<max(superiorCosts) and len(inferior)>2:
                superior.append(destination)
                superiorCosts.append(cost)
                inferior.pop(destinationindex)
                inferiorCosts.pop(destinationindex)
                #this means, that if the destination is good enough to be promoted to become superior, it is deleted from the inferior half and goes to the superior half
        self.population=superior+inferior
        #the population stands for the elements of the halves

def costOfMovement(bacterium):
    #Define cost with very high initial value
    cost=0
    #Define punisher members
    PenaltyFactorForTime=1.7
    PenaltyFactorForDistance=20.3
    #Define test parameters
    InitialSpeeds=[] #initial speed of the robot in m/s
    for i in range(2,12,2):
        InitialSpeeds.append(0.1+0.1*i)
    InitialWallDistances=[] #robot distance from the wall in meters
    for i in range(5,40,5):
        InitialWallDistances.append(0.1+0.1*i)
    InitialWallWidths=[] #wall widths in meters
    for i in range(0,25,5):
        InitialWallWidths.append(0.1+0.1*i)
    #Decode bacterium code to rules and brakepoints
    firstRulePointsNum=bacterium[0]
    firstRulePoints=bacterium[2:(firstRulePointsNum+2)]
    secondRulePointsNum=bacterium[1]
    secondRulePoints=bacterium[(firstRulePointsNum+2):(firstRulePointsNum+secondRulePointsNum+2)]
    controlActionTable=bacterium[(firstRulePointsNum+secondRulePointsNum+2):]
    #All genes will be numbers between -1000 and 1000 so we transform them to fit their appropriate ranges
    for i in range(0,len(firstRulePoints)):
        firstRulePoints[i]=abs(float(firstRulePoints[i])/10)
    firstRulePoints=sorted(firstRulePoints)
    for i in range(0,len(secondRulePoints)):
        secondRulePoints[i]=abs(float(secondRulePoints[i])/1000)
    secondRulePoints=sorted(secondRulePoints)
    for i in range(0,len(controlActionTable)):
        controlActionTable[i]=float(controlActionTable[i])/1000
    #Generation of fuzzy rules
    numberOfSets_1=(firstRulePointsNum-2)/2+2
    numberOfSets_2=(secondRulePointsNum-2)/2+2
    if len(controlActionTable)!=(numberOfSets_1*numberOfSets_2):
        #If the bacterium is invalid, the cost will be returned, that is very high, so this bacterium will probably not survive
        cost=100000
        #print("Invalid bacterium configuration! This one is doomed to extinction!")
        return cost
    setPoints_1=[-101,-100]+copyList(firstRulePoints)+[100000,100001]
    setPoints_2=[-101,-100]+copyList(secondRulePoints)+[100000,100001]
    FirstSetList=[]
    for i in range(0,numberOfSets_1):
        FirstSetList.append(FuzzyRulebook.fuzzySet("FirstSet",setPoints_1[0],setPoints_1[1],setPoints_1[2],setPoints_1[3]))
        setPoints_1.pop(0)
        setPoints_1.pop(0)
    SecondSetList=[]
    for i in range(0,numberOfSets_2):
        SecondSetList.append(FuzzyRulebook.fuzzySet("SecondSet",setPoints_2[0],setPoints_2[1],setPoints_2[2],setPoints_2[3]))
        setPoints_2.pop(0)
        setPoints_2.pop(0)
    actionTable=[]
    dummyTable=copyList(controlActionTable)
    for i in range(0,numberOfSets_2):
        actionTable.append(dummyTable[0:numberOfSets_1])
        for j in range(0,numberOfSets_1):
            dummyTable.pop(0)
    costs=[]
    for speed in InitialSpeeds:
        for distance in InitialWallDistances:
            for width in InitialWallWidths:
                laserData=LaserScan()
                laserData.angle_min=0
                laserData.angle_max=2*math.pi
                laserData.angle_increment=math.pi/180
                laserData.range_min=0.1
                laserData.range_max=3.5
                laserData.ranges=[laserData.range_max*2]*360 #Initialize list with higher elements than the maximum
                #Now the simulation of a brake application is next. We try to inspect if the robot will hit the wall, and if not, how well it will stop
                #First some important parameters are defined
                way=0 #The way that the robot took until stopping
                timestep=1 #Timestep in seconds. It is probably best if I choose this close to the ROS cycle time
                velocity=speed
                timeReq=0
                #Generation of simulated laserdata, when the given wall in the given distance is in front of the robot
                #print("Tic")
                while velocity>0 and way<distance:
                    for data in range(0,len(laserData.ranges)):
                        dist=math.cos(math.radians(data))
                        side=abs(math.tan(math.radians(data))*(distance-way))
                        if dist>0 and side<(width/2): #we only need the elements that represent beams that will hit the wall
                            laserData.ranges[data]=math.sqrt((distance-way)**2+side**2)
                    #print(laserData)
                    safety=FuzzyRulebook.fuzzyfyLidarData(laserData,[[0,90],[270,360]])
                    decision=FuzzyRulebook.controlAction(actionTable,FirstSetList,safety,SecondSetList,velocity)
                    velocity+=decision
                    way+=velocity*timestep
                    timeReq+=timestep
                    #print([way,decision,velocity])
                #print(timestep)
                #print("Tac")
                if way>=distance:
                    #If the robot collides to the wall, a very high cost will be given
                    cost+=1000
                else:
                    cost=cost+PenaltyFactorForDistance*(distance-way)+PenaltyFactorForTime*timeReq
                    costs.append([PenaltyFactorForDistance*(distance-way),PenaltyFactorForTime*timeReq,timeReq])
    cost=cost/(len(InitialSpeeds)*len(InitialWallDistances)*len(InitialWallWidths)) #Simply take the average of all costs
    '''
    print(FirstSetList)
    print(SecondSetList)
    print(actionTable)
    '''
    #print(costs)
    return cost
                
def orderFuzzyBacterium(bacterium):
    #Decode bacterium code to rules and brakepoints
    firstRulePointsNum=bacterium[0]
    firstRulePoints=bacterium[2:(firstRulePointsNum+2)]
    secondRulePointsNum=bacterium[1]
    secondRulePoints=bacterium[(firstRulePointsNum+2):(firstRulePointsNum+secondRulePointsNum+2)]
    controlActionTable=bacterium[(firstRulePointsNum+secondRulePointsNum+2):]
    #All genes will be numbers between -1000 and 1000 so we transform them to fit their appropriate ranges
    for i in range(0,len(firstRulePoints)):
        firstRulePoints[i]=abs(float(firstRulePoints[i])/10)
    firstRulePoints=sorted(firstRulePoints)
    for i in range(0,len(secondRulePoints)):
        secondRulePoints[i]=abs(float(secondRulePoints[i])/1000)
    secondRulePoints=sorted(secondRulePoints)
    for i in range(0,len(controlActionTable)):
        controlActionTable[i]=float(controlActionTable[i])/1000
    ordered=[firstRulePointsNum]+[secondRulePointsNum]+firstRulePoints+secondRulePoints+controlActionTable
    return ordered

def exportFuzzyBacterium(bacterium):
    toExport=orderFuzzyBacterium(bacterium)
    firstRulePointsNum=toExport[0]
    firstRulePoints=toExport[2:(firstRulePointsNum+2)]
    secondRulePointsNum=toExport[1]
    secondRulePoints=toExport[(firstRulePointsNum+2):(firstRulePointsNum+secondRulePointsNum+2)]
    controlActionTable=toExport[(firstRulePointsNum+secondRulePointsNum+2):]
    numberOfSets_1=(firstRulePointsNum-2)/2+2
    numberOfSets_2=(secondRulePointsNum-2)/2+2
    setPoints_1=[-101,-100]+copyList(firstRulePoints)+[100000,100001]
    setPoints_2=[-101,-100]+copyList(secondRulePoints)+[100000,100001]
    FirstSetList=[]
    for i in range(0,numberOfSets_1):
        FirstSetList.append(["FirstSet",setPoints_1[0],setPoints_1[1],setPoints_1[2],setPoints_1[3]])
        setPoints_1.pop(0)
        setPoints_1.pop(0)
    SecondSetList=[]
    for i in range(0,numberOfSets_2):
        SecondSetList.append(["SecondSet",setPoints_2[0],setPoints_2[1],setPoints_2[2],setPoints_2[3]])
        setPoints_2.pop(0)
        setPoints_2.pop(0)
    actionTable=[]
    dummyTable=copyList(controlActionTable)
    for i in range(0,numberOfSets_2):
        actionTable.append(dummyTable[0:numberOfSets_1])
        for j in range(0,numberOfSets_1):
            dummyTable.pop(0)
    FuzzyRulebook.writeSpreadsheetDataFile(FirstSetList,"SafetySet.txt")
    FuzzyRulebook.writeSpreadsheetDataFile(SecondSetList,"MotorSpeedSet.txt")
    print(actionTable)
    FuzzyRulebook.writeSpreadsheetDataFile(actionTable,"ControlActionTable.txt")

def LocalSearchOfMovement(bacterium):
    #Initialize variables. They will probably deleted
    safety=5
    velocity=1
    #Making sets and actiontable (these will probably need to be put in a function)
    toExport=orderFuzzyBacterium(bacterium)
    firstRulePointsNum=toExport[0]
    firstRulePoints=toExport[2:(firstRulePointsNum+2)]
    secondRulePointsNum=toExport[1]
    secondRulePoints=toExport[(firstRulePointsNum+2):(firstRulePointsNum+secondRulePointsNum+2)]
    controlActionTable=toExport[(firstRulePointsNum+secondRulePointsNum+2):]
    numberOfSets_1=(firstRulePointsNum-2)/2+2
    numberOfSets_2=(secondRulePointsNum-2)/2+2
    setPoints_1=[-101,-100]+copyList(firstRulePoints)+[100000,100001]
    setPoints_2=[-101,-100]+copyList(secondRulePoints)+[100000,100001]
    FirstSetList=[]
    for i in range(0,numberOfSets_1):
        FirstSetList.append(["FirstSet",setPoints_1[0],setPoints_1[1],setPoints_1[2],setPoints_1[3]])
        setPoints_1.pop(0)
        setPoints_1.pop(0)
    SecondSetList=[]
    for i in range(0,numberOfSets_2):
        SecondSetList.append(["SecondSet",setPoints_2[0],setPoints_2[1],setPoints_2[2],setPoints_2[3]])
        setPoints_2.pop(0)
        setPoints_2.pop(0)
    actionTable=[]
    dummyTable=copyList(controlActionTable)
    for i in range(0,numberOfSets_2):
        actionTable.append(dummyTable[0:numberOfSets_1])
        for j in range(0,numberOfSets_1):
            dummyTable.pop(0)
    #This is a dummy row. We will need to evaluate a decision using the sets and actiontable
    decision=FuzzyRulebook.controlAction(actionTable,FirstSetList,safety,SecondSetList,velocity)

def costOfMovement_V2(bacterium):
    cost=0
    #Define punisher members
    PenaltyFactorForTime=1.7
    PenaltyFactorForDistance=20.3
    #Define test parameters
    InitialSpeeds=[] #initial speed of the robot in m/s
    for i in range(2,12,2):
        InitialSpeeds.append(0.1+0.1*i)
    InitialWallDistances=[] #robot distance from the wall in meters
    for i in range(5,40,5):
        InitialWallDistances.append(0.1+0.1*i)
    InitialWallWidths=[] #wall widths in meters
    for i in range(0,25,5):
        InitialWallWidths.append(0.1+0.1*i)
    #Decode bacterium code to rules and brakepoints
    numberOfPoints=len(bacterium)/3
    if numberOfPoints!=len(bacterium)//3:
        #If the bacterium is invalid, the cost will be returned, that is very high, so this bacterium will probably not survive
        cost=100000
        #print("Invalid bacterium configuration! This one is doomed to extinction!")
        return cost
    firstRulePoints=bacterium[0:numberOfPoints]
    secondRulePoints=bacterium[numberOfPoints:(2*numberOfPoints)]
    consequents=bacterium[(numberOfPoints*2):]
    #All genes will be numbers between -1000 and 1000 so we transform them to fit their appropriate ranges
    for i in range(0,len(firstRulePoints)):
        firstRulePoints[i]=abs(float(firstRulePoints[i])/10)
    #firstRulePoints=sorted(firstRulePoints)
    for i in range(0,len(secondRulePoints)):
        secondRulePoints[i]=abs(float(secondRulePoints[i])/1000)
    #secondRulePoints=sorted(secondRulePoints)
    for i in range(0,len(consequents)):
        consequents[i]=float(consequents[i])/1000
    #consequents=sorted(consequents)
    #Generation of fuzzy rules
    numberOfSets=numberOfPoints/4
    if numberOfSets!=numberOfPoints//4:
        #If the bacterium is invalid, the cost will be returned, that is very high, so this bacterium will probably not survive
        cost=100000
        #print("Invalid bacterium configuration! This one is doomed to extinction!")
        return cost
    setPoints_1=copyList(firstRulePoints) #+[100000,100001]
    setPoints_2=copyList(secondRulePoints) #+[100000,100001]
    FirstSetList=[]
    for i in range(0,numberOfSets):
        points=sorted(setPoints_1[0:4])
        FirstSetList.append(FuzzyRulebook.fuzzySet("FirstSet",points[0],points[1],points[2],points[3]))
        for j in range(0,4):
            setPoints_1.pop(0)
    SecondSetList=[]
    for i in range(0,numberOfSets):
        points=sorted(setPoints_2[0:4])
        SecondSetList.append(FuzzyRulebook.fuzzySet("SecondSet",points[0],points[1],points[2],points[3]))
        for j in range(0,4):
            setPoints_2.pop(0)
    actionTable=[]
    dummyTable=copyList(consequents)
    for i in range(0,numberOfSets):
        points=sorted(dummyTable[0:4])
        actionTable.append(FuzzyRulebook.fuzzySet("Action",points[0],points[1],points[2],points[3]))
        for j in range(0,4):
            dummyTable.pop(0)
    costs=[]
    laserData=LaserScan()
    laserData.angle_min=0
    laserData.angle_max=2*math.pi
    laserData.angle_increment=math.pi/180
    laserData.range_min=0.1
    laserData.range_max=3.5
    for speed in InitialSpeeds:
        for distance in InitialWallDistances:
            for width in InitialWallWidths:
                laserData.ranges=[laserData.range_max*2]*360 #Initialize list with higher elements than the maximum
                #Now the simulation of a brake application is next. We try to inspect if the robot will hit the wall, and if not, how well it will stop
                #First some important parameters are defined
                way=0 #The way that the robot took until stopping
                timestep=1 #Timestep in seconds. It is probably best if I choose this close to the ROS cycle time
                velocity=speed
                timeReq=0
                #Generation of simulated laserdata, when the given wall in the given distance is in front of the robot
                #print("Tic")
                while velocity>0 and way<distance:
                    for data in range(0,len(laserData.ranges)):
                        dist=math.cos(math.radians(data))
                        side=abs(math.tan(math.radians(data))*(distance-way))
                        if dist>0 and side<(width/2): #we only need the elements that represent beams that will hit the wall
                            laserData.ranges[data]=math.sqrt((distance-way)**2+side**2)
                    #print(laserData)
                    safety=FuzzyRulebook.fuzzyfyLidarData(laserData,[[0,90],[270,360]])
                    decision=FuzzyRulebook.controlAction_V2(FirstSetList,safety,SecondSetList,velocity,actionTable)
                    velocity+=decision
                    way+=velocity*timestep
                    timeReq+=timestep
                    #print([way,decision,velocity])
                #print(timestep)
                #print("Tac")
                if way>=distance:
                    #If the robot collides to the wall, a very high cost will be given
                    cost+=(1000+velocity)
                else:
                    cost=cost+PenaltyFactorForDistance*(distance-way)+PenaltyFactorForTime*timeReq
                    #costs.append([PenaltyFactorForDistance*(distance-way),PenaltyFactorForTime*timeReq,timeReq])
    cost=cost/(len(InitialSpeeds)*len(InitialWallDistances)*len(InitialWallWidths)) #Simply take the average of all costs
    '''
    print(FirstSetList)
    print(SecondSetList)
    print(actionTable)
    '''
    #print(costs)
    return cost

def orderFuzzyBacterium_V2(bacterium):
    #Decode bacterium code to rules and brakepoints
    numberOfPoints=len(bacterium)/3
    firstRulePoints=bacterium[0:numberOfPoints]
    secondRulePoints=bacterium[numberOfPoints:(2*numberOfPoints)]
    consequents=bacterium[(numberOfPoints*2):]
    #All genes will be numbers between -1000 and 1000 so we transform them to fit their appropriate ranges
    for i in range(0,len(firstRulePoints)):
        firstRulePoints[i]=abs(float(firstRulePoints[i])/10)
    #firstRulePoints=sorted(firstRulePoints)
    for i in range(0,len(secondRulePoints)):
        secondRulePoints[i]=abs(float(secondRulePoints[i])/1000)
    #secondRulePoints=sorted(secondRulePoints)
    for i in range(0,len(consequents)):
        consequents[i]=float(consequents[i])/1000
    #Generation of fuzzy rules
    ordered=firstRulePoints+secondRulePoints+consequents
    return ordered

def exportFuzzyBacterium_V2(bacterium):
    toExport=orderFuzzyBacterium_V2(bacterium)
    numberOfSets=(len(bacterium)/3)/4
    FirstSetList=[]
    for i in range(0,numberOfSets):
        points=sorted(toExport[0:4])
        FirstSetList.append(["FirstSet",points[0],points[1],points[2],points[3]])
        for j in range(0,4):
            toExport.pop(0)
    SecondSetList=[]
    for i in range(0,numberOfSets):
        points=sorted(toExport[0:4])
        SecondSetList.append(["SecondSet",points[0],points[1],points[2],points[3]])
        for j in range(0,4):
            toExport.pop(0)
    actionTable=[]
    for i in range(0,numberOfSets):
        points=sorted(toExport[0:4])
        actionTable.append(["Action",points[0],points[1],points[2],points[3]])
        for j in range(0,4):
            toExport.pop(0)
    FuzzyRulebook.writeSpreadsheetDataFile(FirstSetList,"SafetySet_V2.txt")
    FuzzyRulebook.writeSpreadsheetDataFile(SecondSetList,"MotorSpeedSet_V2.txt")
    print(actionTable)
    FuzzyRulebook.writeSpreadsheetDataFile(actionTable,"ActionSet_V2.txt")

def costOfSynapse(bacterium):
    tau_FT=float(bacterium[0])/10
    if tau_FT==0:
        tau_FT=0.1
    X_ext=float(bacterium[1])/1000
    P_PSP=float(bacterium[2])/1000
    X=0
    V=0
    t_imp=0
    E=P_PSP-P_PSP*math.exp((-1)*0.1/tau_FT)
    for i in range(0,10):
        x_PSP=P_PSP*math.exp((-1)*t_imp/tau_FT)
        A=7*X*x_PSP
        V+=V+E-X_ext
        X+=X+V-A
        if X<0:
            return 1000
        t_imp+=0.1
    return abs(1-X)

"""
example=BacterialMemeticOptimization(enableDifferentLength=True)
example.InitialPopulation(5,10)
print(example.population)
for i in range(0,10):
    example.Mutation(10)
    example.GeneTransfer(10,1)

print(example.population)
"""
#OhWell=costOfMovement([10,9,4,5,6,7,8,9,10])
#OhWell=costOfMovement([8,4,6,8,12,14,24,26,58,60,0.1,0.2,0.4,0.5,0.1,0.07,0.035,0,-0.035,0.07,0.035,0,-0.035,-0.07,0,-0.035,-0.07,-0.105,-0.14])
#OhWell=random.randint(-10,10)
#OhWell=costOfMovement([8,4,60,80,120,140,240,260,580,600,100,200,400,500,100,70,35,0,-35,70,35,0,-35,-70,0,-35,-70,-105,-140])

'''
example=BacterialMemeticOptimization(geneMinimum=0,geneMaximum=1000,costfunction=costOfSynapse)
example.InitialPopulation(3,10)
print(example.population)
for i in range(0,10):
    example.Mutation(100)
    example.GeneTransfer(100,1)
for i in example.population:
    print(costOfSynapse(i))
print(example.population)
'''
"""
graphs=[]
start=time.time()
BasicElement=[8,4,60,80,120,140,240,260,580,600,100,200,400,500,100,70,35,0,-35,70,35,0,-35,-70,0,-35,-70,-105,-140]
print("Cost of the initial element")
print(BasicElement)
print(costOfMovement(BasicElement))
print("Results of the algorithm")
BasicPopulation=[]
for i in range(0,5):
    BasicPopulation.append(copyList(BasicElement))
test=BacterialMemeticOptimization(geneMinimum=-1000,geneMaximum=1000,costfunction=costOfMovement,initialpopulation=BasicPopulation)
for i in range(0,10):
    test.Mutation(5)
    print("Mutation is done!")
    test.GeneTransfer(20,1)
    print("One cycle is done!")
    end=time.time()
    print(end-start)
    costs=[]
    for i in test.population:
        print(orderFuzzyBacterium(i))
        cost=costOfMovement(i)
        costs.append(cost)
        print(cost)
    graphs.append(costs)
exportFuzzyBacterium(test.population[0])
print(end-start)
FuzzyRulebook.writeSpreadsheetDataFile(graphs,"Graphs.txt")
"""

'''
BasicElement=[8,4,60,80,120,140,240,260,580,600,100,200,400,500,100,70,35,0,-35,70,35,0,-35,-70,0,-35,-70,-105,-140]
print("Cost of the initial element")
print(BasicElement)
print(costOfMovement(BasicElement))
print("Results of the algorithm")
BasicPopulation=[]
for i in range(0,5):
    BasicPopulation.append(copyList(BasicElement))
'''



graphs=[]
start=time.time()
test=BacterialMemeticOptimization(geneMinimum=-1000,geneMaximum=1000,costfunction=costOfMovement_V2)
test.InitialPopulation(60,10)
for i in range(0,3):
    test.Mutation(4)
    print("Mutation is done!")
    test.GeneTransfer(4,1)
    print("One cycle is done!")
    end=time.time()
    print(end-start)
    costs=[]
    for i in test.population:
        print(orderFuzzyBacterium_V2(i))
        cost=costOfMovement_V2(i)
        costs.append(cost)
        print(cost)
    graphs.append(costs)
exportFuzzyBacterium_V2(test.population[0])
end=time.time()
print(end-start)
FuzzyRulebook.writeSpreadsheetDataFile(graphs,"Graphs_V2.txt")

"""
tomb=[]
for i in range(60):
    tomb.append(random.randint(-1000,1000))
print(tomb)
print(sorted(tomb[0:4]))
print(orderFuzzyBacterium_V2(tomb))
print(costOfMovement_V2(tomb))
"""
#print(OhWell)
#this was an experiment to test the algorithm. It is a simple task, but it really seems, that this algorithm does its job very well

