#! /usr/bin/env python
import FuzzyRulebook
import os
import math
import random


robotSize=0.21 #meter
calcSpeedFactor=8 #az utkereses lassu, ha pixelre pontosan meg akarjuk mondani a celt, igy inkabb kisebb felbontast hasznalunk

def quaternion_to_euler(x, y, z, w):
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    X = math.degrees(math.atan2(t0, t1))
    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    Y = math.degrees(math.asin(t2))
    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    Z = math.degrees(math.atan2(t3, t4))
    return [X,Y,Z]

def getSlamId():
    f=os.popen("ps")
    processes=f.read()
    print(processes)
    processList=processes.split("\n")
    gmapId=0
    for x in processList:
        if 0<=x.find("slam"):
            idEnding=x.find(" ")
            gmapId=x[0:idEnding]
            print(gmapId)
            return gmapId

def pauseMapping(gmapId):
    os.system("kill -STOP "+gmapId)

def continueMapping(gmapId):
    os.system("kill -CONT "+gmapId)

def reduceMapResolution(mapData,changeFactor,additionalDistance=0):
    oldWidth=mapData.info.width
    oldData=mapData.data
    mapData.info.width=mapData.info.width//changeFactor
    mapData.info.height=mapData.info.height//changeFactor
    mapData.info.resolution=mapData.info.resolution*changeFactor
    newData=[]
    for x in range(mapData.info.width*mapData.info.height):
        newData.append([])
    for x in range(mapData.info.height):
        startY=x*mapData.info.width
        for y in range(mapData.info.width):
            startX=y
            for w in range(changeFactor):
                startIndex=x*changeFactor*oldWidth+w*oldWidth+changeFactor*y
                for z in range(changeFactor):
                    newData[x*mapData.info.width+y].append(mapData.data[startIndex+z])
    mapData.data=[]
    for x in newData:
        if 100 in x:
            mapData.data.append(100)
        elif (x.count(0)*1)>=x.count(-1):
            mapData.data.append(0)
        else:
            mapData.data.append(-1)
    #Ezutan adunk egy kis rahagyast, koltseget a falak melletti helyekre hogy a robot ne mehessen tul kozel a falakhoz
    for i in range(0,additionalDistance): #Annyi mezo a biztonsagi tavolsag, amennyi az argumentum
        modified=[]
        for x in range(0,len(mapData.data)):
            possibleNeighbors=[x-1,x+1,x+mapData.info.width,x-mapData.info.width,x+mapData.info.width+1,x+mapData.info.width-1,x-mapData.info.width+1,x-mapData.info.width-1]
            for y in possibleNeighbors:
                try:
                    if not y in modified:
                        if mapData.data[y]==100:
                            mapData.data[x]=100 # Ha a szomszed meg nincs modositva, es fal, akkor a vizsgalt mezo is fal lesz es modositottak listajaba kerul
                            modified.append(x)
                except:
                    pass #Ha kimegyunk a listabol, siman megyunk tovabb
    FuzzyRulebook.writeSpreadsheetDataFile([mapData.data],"NewMap.txt")


def transformFoundPathToTheOriginalMap(originalmapInfo,incrementFactor,foundPathPoints):
    originalPathPoints=[]
    oldWidth=originalmapInfo.width//incrementFactor
    oldHeight=originalmapInfo.height//incrementFactor
    for x in foundPathPoints:
        origX=(x%oldWidth)*incrementFactor+(incrementFactor//2)
        origY=(x//oldWidth)*incrementFactor+(incrementFactor//2)
        newPoint=origY*originalmapInfo.width+origX
        originalPathPoints.append(newPoint)
    return originalPathPoints

def odomOnMap(odomData,mapInfo):
    xOnMap=int((odomData.pose.pose.position.x/mapInfo.resolution)+(mapInfo.width/2))
    yOnMap=int((odomData.pose.pose.position.y/mapInfo.resolution)+(mapInfo.height/2))
    elementNumber=yOnMap*mapInfo.width+xOnMap
    #ez adja meg, hanyadik elem a map listaban a megadott pozicio
    return elementNumber


class pathSegment():
    def __init__(self,placeOnTheMap,mapInfo):
        self.f=0
        self.g=0
        self.h=0
        self.neighbors=[]
        self.previous=None
        self.place=placeOnTheMap
    def defineNeighbors(self,allSegments,mapInfo):
        #Megadjuk a szomszedjait, de csak akkor, ha nincs a szelen
        #Az if-ek felteteleiben azt nezzuk, szelen van-e
        if self.place%mapInfo.width>0:
            self.neighbors.append(allSegments[self.place-1])
            #Atlos szomszedok:
            if self.place>=mapInfo.width:
                self.neighbors.append(allSegments[self.place-1-mapInfo.width])
            if self.place<(mapInfo.width*(mapInfo.height-1)):
                self.neighbors.append(allSegments[self.place-1+mapInfo.width])
        if self.place%mapInfo.width!=(mapInfo.width-1):
            self.neighbors.append(allSegments[self.place+1])
            #Atlos szomszedok:
            if self.place>=mapInfo.width:
                self.neighbors.append(allSegments[self.place+1-mapInfo.width])
            if self.place<(mapInfo.width*(mapInfo.height-1)):
                self.neighbors.append(allSegments[self.place+1+mapInfo.width])
        if self.place>=mapInfo.width:
            self.neighbors.append(allSegments[self.place-mapInfo.width])
        if self.place<(mapInfo.width*(mapInfo.height-1)):
            self.neighbors.append(allSegments[self.place+mapInfo.width])

def A_star(mapData,odomDataStart,odomDataGoal,calcSpeedFactor,usingReducedMap):
    #Ha mar eleve csokkentett felbontasu terkepet kaptunk (pl. gyorsabb szamolas miatt), akkor nem csinaljuk meg magunknak a csokkentest
    if not usingReducedMap:
        reduceMapResolution(mapData,calcSpeedFactor,0)
    print("Reduction of resolution is complete")
    openSet=[]
    closedSet=[]
    pathSegmentSet=[]
    path=[]
    for x in range(len(mapData.data)):
        pathSegmentSet.append(pathSegment(x,mapData.info))
    for y in pathSegmentSet:
        y.defineNeighbors(pathSegmentSet,mapData.info)
    startPos=pathSegmentSet[int(odomOnMap(odomDataStart,mapData.info))]
    goalPos=pathSegmentSet[int(odomOnMap(odomDataGoal,mapData.info))]
    print("Pathsegments are done!")
    print(startPos.place)
    print(goalPos.place)
    if mapData.data[goalPos.place]<=0:
        openSet.append(startPos)
    else:
        print("I can not reach the given goal position, because it is a wall!")
    wayFound=False
    count=0
    while len(openSet)>0 and not wayFound and count<2000:
        count=count+1
        lowestIndex=0
        for x in range(len(openSet)):
            if openSet[x].f<openSet[lowestIndex].f:
                lowestIndex=x
        current=openSet[lowestIndex]
        print(current.place)
        if current.place==goalPos.place:
            wayFound=True
            temp=current
            path.append(temp.place)
            while temp.previous!=None:
                path.append(temp.previous.place)
                temp=temp.previous
        closedSet.append(current)
        openSet.remove(current)
        for x in current.neighbors:
            if x not in closedSet and mapData.data[x.place]<=0:
                #lehet atlosan is lepni, de annak nagyobb a tavolsaga
                if 1!=abs(current.place-x.place) and abs(current.place-x.place)!=mapData.info.width:
                    g_temp=current.g+math.sqrt(2)
                else:
                    g_temp=current.g+1
                newPath=False
                if x in openSet:
                    if g_temp<x.g:
                        x.g=g_temp
                        newPath=True
                else:
                    x.g=g_temp
                    newPath=True
                    openSet.append(x)
                if newPath:
                    x.h=math.sqrt(((x.place%mapData.info.width)-(goalPos.place%mapData.info.width))**2+((x.place//mapData.info.width)-(goalPos.place//mapData.info.width))**2)
                    x.f=x.g+x.h
                    x.previous=current
    if wayFound:
        print("Path found!")
        print(count)
    else:
        print("I can not reach the given goal position! How is that?")
    path.reverse()
    FuzzyRulebook.writeSpreadsheetDataFile([path],"NewPath.txt")
    mapData.info.width=mapData.info.width*calcSpeedFactor
    mapData.info.height=mapData.info.height*calcSpeedFactor
    mapData.info.resolution=mapData.info.resolution/float(calcSpeedFactor)
    path=transformFoundPathToTheOriginalMap(mapData.info,calcSpeedFactor,path)
    return path

def copyList(listToCopy):
    #sadly python2 does not have builtin function for copying a list, so I try to write one
    #This was also declare at the Optimization.py, but they belong to different projects, so we handle them differently
    oldList=listToCopy
    newList=[]
    for i in range(0,len(oldList)):
        newList.append(oldList[i])
    return newList

def signum(a):
    #There is no signum function in python2. Too bad
    sign=0
    if a>0:
        sign=1
    elif a<0:
        sign=-1
    else:
        sign=0
    return sign

def sigmoid(x):
    y=1/(1+math.exp(-1*x))
    return y

def randomMap(width,height,isThreeD=False,chanceForWall=0):
    createdMap=[]
    for i in range(0,width):
        mapRow=[]
        for j in range(0,height):
            decision=0
            if isThreeD==True:
                decision=random.randint(0,100)
            else:
                if random.randint(0,100)<chanceForWall:
                    decision=100
            mapRow.append(decision)
        createdMap.append(mapRow)
    return createdMap

class NeuroActivityBasedDynamicPathPlanner():
    def __init__(self,robotMass,mapData,mapWidth=1):
        self.mapData=[]
        self.mapWidth=0
        self.checkMapInfo(mapData,mapWidth) #This function will handle the map
        self.startpoint=0
        self.goalpoint=0
        self.path=[] #This represents the planned path. Contains the neuron ID-s
        #Some physical parameters are given
        self.m=robotMass #mass of the robot
        self.g=9.81 #gravity acceleration
        #We determine default parameters
        self.d_max=1.2 #maximum influence of the external neuros on the spike signal
        self.P_psp=30
        self.gamma_syn=0.05
        self.gamma_ref=0.2
        self.gamma_inf=1.5 #This is the strength of the scope. When searching for possible connections, the bigger this is, the more connections can be made. This must not be too big or too small
        #Later we must give gamma_inf a better determination, when we know the map representation
        self.signalFadingTimeout=1000 #This is a tmeout parameter that prevents memory leak. If the signal can not reach the target until timeout, it will fade and die
        self.CycleTime=0.5 #This is the time resolution of both spiking models is seconds
        self.ElapsedTime=0 #We also measure elapsed time according to cycle time, but we might only use it in backward transmission
        self.releaseEnergyFactor=1 #This influences the release energy at forward transmission
        self.travelTimeFactor=30 #This influences the weight calculation from travel time at backward transmission
        self.reverseTravelTimeDependency=True #If this is true, more travel time will mean less chance for synaptic pruning, and vica versa
        self.percentualSynapseRemoval=50 #Percent of maximum synapses that can be removed during backward transmission
        self.R=10
        self.theta=0.8
        self.alpha_l=0.3
        self.alpha_d=0.7
        self.alpha=0.5 #This is the degradation dimension parameter for synaptic pruning. Must be from 0 to 1
        self.eta=0.8 #This is the threshold value. The smaller it is, the faster the neuron is getting aged and unusable
        self.f0_ft=1 #This represents the input field of a neuron proposed by Chechik. In this model we set this to 1, but I write it here as a variable
        self.NeuronSet=[] #This property is used to collect neurons in one place. The neurons represent a segment of the map. The set starts as empty, but it will be filled shortly
        self.SignalSet=[] #This property is used to collect spike signals in one place
        self.X=[] #X is a matrix, that stores the signal diversities between neurons. This matrix has as many rows and columns, as many neurons there are
        self.X_ext=[] #This matrix represents the external influence between two neurons. The goal is the optimization of the time complexity
        '''
        self.V=[] #Potential force between two neurons
        #Actually I am not sure if all of the following properies are needed, we will find it out later
        self.s_unp=[] #Unpredictable external effect between two neurons
        self.s_w=[] #Weight force between two neurons
        self.s_f=[] #Frictional force between two neurons
        self.s_s=[] #Effect of other neurons for each connection
        '''
    def checkMapInfo(self,mapData,mapWidth):
        #Inspect map information
        try:
            #First we suppose that the array is 2 dimensional and the first row is the top of the map in this case we must transform it to 1D array
            mapDataOriginal=list(reversed(mapData))
            self.mapData=[]
            for i in mapDataOriginal:
                self.mapWidth=len(i) #we check the width of the map. If this block fails here, we suppose, that the developer gave 1D map originally
                for j in i:
                    self.mapData.append(j)
        except:
            self.mapData=copyList(mapData) #WARNING: This will not copy the array, it will be at the same place in the memory
            self.mapWidth=mapWidth
        diviation=(float(len(self.mapData))/self.mapWidth)
        quotient=(float(len(self.mapData))//self.mapWidth)
        if diviation!=quotient:
            print("WARNING! The map configuration is invalid! The 1D array size or the width is wrong! Please check these!")
            missing=self.mapWidth-(float(len(self.mapData))%self.mapWidth)
            self.mapData=self.mapData+[0]*missing #We extend the list with zeros to be able to run the algorithm
        if self.mapWidth==1:
            print("WARNING! The width of the map is equal to 1. This can cause inefficient pathplanning! If you give a 1D array as parameter, consider to give mapWidth parameter as well! If you give a 2D array, mapWidth parameter is not mandatory!")
    def transformStartAndGoal(self):
        try:
            self.startpoint=self.startpoint[1]*self.mapWidth+self.startpoint[0]
        except:
            if self.startpoint>len(self.mapData):
                print("Invalid startpoint!")
        try:
            self.goalpoint=self.goalpoint[1]*self.mapWidth+self.goalpoint[0]
        except:
            if self.goalpoint>len(self.mapData):
                print("Invalid goalpoint!")
    class Neuron():
        def __init__(self,id,x,y,z,friction):
            #Parameters of forward transmission
            self.ID=id #The unique identifier of the neuron
            self.coordinates=[x,y,z] #The coordinates of the neuron
            self.x_PSP=0 #The potential energy of the neuron
            self.t_imp=10000 #This parameter will determine signal travel time. This starts at 10000
            self.C_i=[0] #Special array. It represents the connections of the specific neuron. The 0th element is the number of connections, the others are the IDs of the connectable neurons. We give it the 0th element, that starts as zero
            self.frictionForce=friction #Gives the value of friction force on the given mappoint
            #Parameters of backward transmission
            self.W=100 #This is the age of the neuron. When it reaches zero, the neuron becomes unused, and it loses its connections
            self.h=0
            self.h_ref=0.1
            self.h_syn=0
            self.h_PSP=0 #Post synaptic potential, like x_PSP, but this is for backward transmission
            self.p=0 #Indicates if the pulse signal is released
            self.t_f=0 #Last time of firing
            self.tau_FT=10 #There is a tau member for both models. They might have different values
            self.tau_BT=10
            self.presynaptic=-1 #The previous neuron in the chain
            self.T=0 #Travel time of the signal, that made this neuron firing
    class SpikeSignal():
        def __init__(self,sourceNeuron,targetNeuron):
            self.source=sourceNeuron #The ID of the neuron, that emits the spike signal
            self.target=targetNeuron #The ID of the neuron, the spike signal is heading to
            self.coordinates=[self.source.coordinates[0],self.source.coordinates[1],self.source.coordinates[2]] #The coordinates of the signal. When an object is created, the signal starts from the source neuron. The source neuron can be given this way, no need to use the function to get it
            self.E=0 #Release energy. This is more of a signal property, than a neuron property
            self.T=0 #This is the travel time of the signal
            #self.X_ext=0 #This is the external influence effecting this signal
            self.V=0 #This is the potential force. We try to assign it to the signal
            self.speed=0.1 #Step per seconds. This is the speed, the neuron is moving with
    def initializeNeuronSet(self):
        #initialize forward transition
        #We suppose that the map is already a 1D array, the 0th element is the bottom left, the last one is the top right corner
        self.X=[]
        self.X_ext=[]
        self.NeuronSet=[]
        for i in range(0,len(self.mapData)):
            self.X.append([0]*len(self.mapData)) #We must fill X with zeros at initial state
            #self.V.append([0]*len(self.mapData)) #We must fill V with zeros at initial state
            self.X_ext.append([0]*len(self.mapData)) #We must fill X_ext with zeros at initial state
            self.NeuronSet.append(self.Neuron(i,i%self.mapWidth,i//self.mapWidth,self.mapData[i],self.predictFrictionForce())) #Transform mappoint to neuron. ID is the element index, z is the value, and if we divide the index with the width, x is the remainder and y is the quotient
    def predictFrictionForce(self):
        frictionForce=0 #This function will make the prediction of the friction force. Not implemented yet
        return frictionForce
    def neuronDistance(self,n1,n2):
        distance=0
        coords=[]
        for i in range(0,len(n1.coordinates)):
            diff=(n2.coordinates[i]-n1.coordinates[i])
            coords.append(diff)
            distance=distance+diff**2
        distance=math.sqrt(distance)
        return distance,coords #two variables are returned
    def getNeuronByID(self,ident):
        #For time complexity reasons this function became unused
        index=0
        found=False
        while found==False and index<len(self.NeuronSet):
            if self.NeuronSet[index].ID==ident:
                found=True
            else:
                index+=1
        if found==False:
            print("ERROR! There is no neuron with the given ID")
        return self.NeuronSet[index] #The neuron itself is returned
    def getSignalByNeuronIDs(self,sourceID,targetID):
        index=0
        found=False
        while found==False and index<len(self.SignalSet):
            if self.SignalSet[index].source.ID==sourceID and self.SignalSet[index].target.ID==targetID:
                found=True
            else:
                index+=1
        if index==len(self.SignalSet):
            index=-1
        return index # returns -1 if there is no signal between the given neurons, and the index of the signal if there is
    def buildPath(self):
        path=[]
        if self.NeuronSet[self.goalpoint].presynaptic==-1:
            print("The goal point can not be reached! No path can be built!")
        else:
            done=False
            currentID=self.goalpoint
            while done==False:
                path.append(currentID)
                if currentID!=self.startpoint:
                    currentID=self.NeuronSet[currentID].presynaptic
                else:
                    done=True
        self.path=path
    def calculate_s_w(self,n1,n2):
        dist,coord=self.neuronDistance(n1,n2)
        direction=[0,0,1]
        summa=0
        for i in range(0,len(direction)):
            summa+=direction[i]*(coord[i]/dist)
        sw=self.m*self.g*summa
        return sw
    def calculate_s_f(self,n1,n2):
        sf=(n1.frictionForce+n2.frictionForce)/2 #The prediction is taking the average of the friction of the neurons. This is very unaccurate, but it can be a future improvement to make it more accurate
        return sf
    def calculate_s_s(self,signal):
        ss=0
        n1=signal.source
        n2=signal.target
        coords=copyList(signal.coordinates)
        dist,distcoordinates=self.neuronDistance(n1,n2)
        radius=dist/2 #The influenced area is within a circle with radius of dist/2
        coordsOfMiddle=[]
        usedCoords=2 #We use only the x and y coordinates to check if the neuron can influence the signal travel
        for i in range(0,usedCoords):
            avg=(n1.coordinates[i]+n2.coordinates[i])/2
            coordsOfMiddle.append(avg)
        #closeNeuronIndexes=[]
        for i in range(0,len(self.NeuronSet)):
            distance=0
            for j in range(0,usedCoords):
                distance+=(coordsOfMiddle[j]-self.NeuronSet[i].coordinates[j])**2
            distance=math.sqrt(distance) #Calculates the distance of the signal and the picked neuron
            if distance<=radius:
                #closeNeuronIndexes.append(i) #We gather the indexes of the neurons, that are close enough to the spike signal
                distanceFromTargetCoords=[]
                distanceFromExternalCoords=[]
                distanceFromExternal=0
                for k in range(0,len(coords)):
                    distanceFromTargetCoords.append(n2.coordinates[k]-coords[k]) #Distance of signal and target neuron (all coordinates are needed)
                    distanceFromExternalCoords.append(self.NeuronSet[i].coordinates[k]-coords[k]) #Distance of signal and external neuron (all coordinates are needed)
                    distanceFromExternal+=(self.NeuronSet[i].coordinates[k]-coords[k])**2
                distanceFromExternal=math.sqrt(distanceFromExternal)
                h_Cjk=0
                D_Cjk=0
                direction=[0,0,1]
                for k in range(0,len(distanceFromExternalCoords)):
                    h_Cjk+=distanceFromExternalCoords[k]*direction[k]
                    D_Cjk+=distanceFromTargetCoords[k]*distanceFromExternalCoords[k]
                exponentialMember=math.exp((-1)*distanceFromExternal/self.d_max)
                ss+=D_Cjk*h_Cjk*exponentialMember
                #print([D_Cjk,h_Cjk,exponentialMember])
        return ss
    def getPossibleConnections(self,neuron): #Named as Algortithm 2 in the article
        #This function gives the possible connection of the neuron given by the parameter
        neuron.C_i=[0] 
        k=0 #This will be the number of connections
        for j in self.NeuronSet:
            if neuron.ID!=j.ID:
                d,coord=self.neuronDistance(neuron,j)
                if d<=self.gamma_inf:
                    #If the distance is small enough, the two neurons may connect. Then the ID of j is written in C_i, and the number of connection is increased
                    neuron.C_i.append(j.ID)
                    k=k+1
        neuron.C_i[0]=k
        #return C_i #NOT Return the special array, that contains the number of connections, and the IDs of connectable neurons
    def ForwardTransmission(self,epochs): #Named as Algortithm 1 in the article
        self.initializeNeuronSet() #Initialization, creating neurons from mapdata, signal diversity X is zero between all neurons, x_PSP is 0 and t_imp is 10000 for all neurons
        #print(self.NeuronSet[0].ID)
        self.transformStartAndGoal() #We need to transform the start and tartget to our needs
        for n in self.NeuronSet:
            self.getPossibleConnections(n) #Find the possible connections for each neuron
        self.Spiking_FT(self.NeuronSet[self.startpoint]) #At the start of forward transmission the starting neuron spikes
        #print(self.NeuronSet[self.startpoint].C_i)
        e=0
        #for e in range(0,epochs):
        while e<epochs and self.NeuronSet[self.goalpoint].presynaptic==-1:
            #self.SignalMovement()
            self.SignalPosition()
            self.signalEvaluation_FT()
            #print(e)
            e+=1
            #print(len(self.SignalSet))
        self.buildPath()
        print(self.path)
        
    def signalEvaluation_FT(self):
        '''
        for n in self.NeuronSet: #We do the following for each neuron
            for i in range(1,(n.C_i[0]+1)): #We do the following for each connection of this neuron
                j=n.C_i[i]
                F=self.SignalSet[self.getSignalByNeuronIDs(n,self.NeuronSet[j])] #F probably indicates if a spkie signal is flowing between the two neurons
                '''
                #if we uncomment these, everything below must be pushed to right
        for s in self.SignalSet:
            #First the external influence must be calculated
            n=s.source
            j=s.target #we give data like that for further use
            #Calculate X_ij_ext by equation (7)
            n_j=j #self.NeuronSet[j] #we call the jth neuron, so we can use it later
            s_w=self.calculate_s_w(n,n_j) #calculate weight force
            s_unp=0 #TODO: Find out how to calculate this
            s_f=self.calculate_s_f(n,n_j) #calculate friction force. It is constant value for now, we will try to predict it later
            s_s=self.calculate_s_s(s) #calculate other neurons effect, but only those, that are in the influenced area
            self.X_ext[n.ID][j.ID]=s_w+s_unp+s_f+s_s
        for s in self.SignalSet:
            n=s.source
            j=s.target
            n_j=j
            #Calculate X_ij by equation (1)
            #A=(sum(self.X[n_j.ID])-self.X[n_j.ID][n_j.ID])*n_j.x_PSP
            A=(sum(self.X_ext[n.ID])-self.X_ext[n.ID][n.ID])*n.x_PSP #Calculate the inhibitory effect. Get the sum of one row of the matrix, subtract the diversity compared to itself (if it wouldn't be 0 somehow), and multiply with the potential energy
            #WARNING! The source article contains the wrong formula for A. X_ext must be used instead of X
            '''
            V_derived=(self.V[n.ID][j.ID]+(s.E-X_ext[n.ID][j.ID]))*self.CycleTime #Calculate the derived of the potential force. Still not clear what derived means. E will be calculated later. It probably gains value when the neuron is firing
            self.V[n.ID][j.ID]+=V_derived
            X_derived=(self.X[n.ID][j.ID]+self.V[n.ID][j.ID]-A)*self.CycleTime
            '''
            V_derived=(s.V+(s.E-self.X_ext[n.ID][j.ID]))*self.CycleTime #Calculate the derived of the potential force. Still not clear what derived means. E will be calculated later. It probably gains value when the neuron is firing
            s.V+=V_derived
            X_derived=(self.X[n.ID][j.ID]+s.V-A)*self.CycleTime
            self.X[n.ID][j.ID]+=X_derived
            #next step is to calculate eq. 3, 4 and 5. Then continue with the algorithm
            distance,distcoords=self.neuronDistance(n,n_j)
            s.T+=self.CycleTime #Increase the travel time
            #print([self.X[n.ID][j.ID],s.V,A])
            #print([self.X[n.ID][j.ID],self.V[n.ID][j.ID],A])
            #print(X_ext[n.ID][j.ID])
            #print([s_f,s_w,s_s])
            #print([self.X[n.ID][j.ID],distance])
            if n_j.presynaptic!=-1:
                self.SignalSet.remove(s) #If the postsynaptic neuron already took part in pathplanning, we do not need this signal any more
            elif self.X[n.ID][j.ID]>=distance: #and n_j.t_imp>0: #The second part of the statement is the refractoryness. It is not sure, that this is the correct action for it
                #The jth neuron fires impulse signal
                #print([self.X[n.ID][j.ID],distance])
                #print(s.source.ID)
                self.Spiking_FT(n_j)
                n_j.T=s.T
                n_j.presynaptic=n.ID #The postsynaptic neuron gets the information about the presynaptic neuron and the travel time
                self.X_ext[n.ID][j.ID]=0 #Open point: do we need to make the X_ext zero after the signal reaches the goal?
                self.SignalSet.remove(s) #Open point: do we have to delete the signal, after it's target neuron fires?
            else:
                n_j.t_imp+=self.CycleTime
                n_j.x_PSP=self.P_psp*math.exp((-1*n_j.t_imp)/n_j.tau_FT)
                if s.T>self.signalFadingTimeout:
                    self.SignalSet.remove(s) #Open point: do we have to delete the signal, after when it can not reach the target
    def SignalMovement(self):
        #print("Signal movement now")
        for s in self.SignalSet:
            target=s.target
            #print(s.sourceID)
            dist,coords=self.neuronDistance(s.source,target)
            distFromTarget=0
            for i in range(0,len(coords)):
                s.coordinates[i]+=coords[i]/dist*s.speed*self.CycleTime #The spike signal moves according to the speed and Cycletime
                distFromTarget+=(s.coordinates[i]-target.coordinates[i])**2
            distFromTarget=math.sqrt(distFromTarget)
            #print([s.coordinates,target.coordinates,coords])
            #print(distFromTarget)
            if distFromTarget<=s.speed*self.CycleTime:
                #print(s)
                self.SignalSet.remove(s) #If the signal gets close to the target, the signal is removed
    def SignalPosition(self):
        #This gives the signal position by X. I am not sure, if this is correct, but it would not surprise me
        for s in self.SignalSet:
            dist,coords=self.neuronDistance(s.source,s.target)
            for i in range(0,len(coords)):
                s.coordinates[i]=(min([self.X[s.source.ID][s.target.ID],0])/dist)*coords[i]+s.source.coordinates[i]
    def Spiking_FT(self,neuron):
        #print(neuron.ID)
        #print(len(self.SignalSet))
        t_diff=neuron.t_imp
        neuron.t_imp=0 #t_imp of the jth neuron is zero
        psp_original=neuron.x_PSP
        neuron.x_PSP=self.P_psp*math.exp((-1*neuron.t_imp)/neuron.tau_FT)
        x_diff=neuron.x_PSP-psp_original
        for k in range(1,(neuron.C_i[0]+1)):
            neighbor=self.NeuronSet[neuron.C_i[k]]
            if neighbor.presynaptic==-1:
                newSignal=self.SpikeSignal(neuron,neighbor) #If the neighbor neuron did not take part at pathplanning so far, it can revieve new signal
                newSignal.E=(x_diff/t_diff)*self.releaseEnergyFactor #The release energy depends on the derivative of potential energy degradation at time signal impulse
                self.SignalSet.append(newSignal) #New signal is released to all neighbors of the jth neuron
    def calculateWeightFromTravelTime(self,traveltime):
        weight=max([traveltime,0])
        if self.reverseTravelTimeDependency==True:
            weight=1/weight
        weight*=self.travelTimeFactor
        return weight
    def AgeStep(self,neuron):
        #This function requires the presynaptic and the postsynaptic neuron
        #The age applies to the neuron, and not to its connections, but it is not surely correct
        #minDist=100000 #minimal distance between two neurons
        #minDistCoords=[-1,-1,-1]
        neuronID=neuron.ID
        index=-1
        try:
            index=self.path.index(neuronID)
        except:
            pass
        if index==0 or index==(len(self.path)-1):
            index=-1 #We do not evaluate the age of the start- and goalpoint. They can not be taken out from the path
        else:
            n_post=self.NeuronSet[self.path[index+1]]
            n_ext=self.NeuronSet[self.path[index-1]]
            #We must claim the sorrounding neurons
        '''
        for neighborID in range(1,len(n_post.C_i)):
            if not neighborID in neuron.C_i[1:]:
                dist,coords=self.neuronDistance(self.NeuronSet[neighborID],neuron)
                #We might need to calculate the distance from target point, but we try it later
                #distfromgoal,distfromgoalcoords=self.neuronDistance(self.NeuronSet[neighborID],self.NeuronSet[self.goalpoint])
                diff=dist #+distfromgoal
                if diff<minDist:
                    #We need to find the neuron, that is connected to the postsynaptic but not connected to the presynaptic. If there are multiple of them, we need the one closest to the presynaptic
                    minDist=dist
                    minDistCoords=copyList(coords)
                    neuronID=neighborID
        '''
        if index>=0:
            #if no neuron is foud, that fits the requirement, no synaptic pruning can be performed, and even the age can not change
            dist,coords=self.neuronDistance(n_ext,n_post)
            #Now we calculate beta_l by equation (14)
            beta_l=dist #...-distance from the goal
            #Now we calculate beta_d by equation (15)
            #n_ext=self.NeuronSet[neuronID]
            dist_ik,coords_ik=self.neuronDistance(n_ext,neuron)
            #print([coords_ik,coords])
            n=[]
            for i in range(0,len(coords)):
                n.append(coords[i]/dist) #Creating the unit vector n
            vectorproduct=0
            for i in range(0,len(n)):
                vectorproduct+=n[i]*coords_ik[i]
            beta_d=0
            for i in range(0,len(n)):
                beta_d+=(coords_ik[i]-(vectorproduct*n[i]))**2
            beta_d=math.sqrt(beta_d)
            #print(n)
            #Now calculate epsilon by equation (13)
            epsilon=neuron.h_PSP*(self.alpha_l*beta_l+self.alpha_d*beta_d)
            #epsilon=1*(self.alpha_l*beta_l+self.alpha_d*beta_d)
            mu=sigmoid(self.eta-epsilon)
            #mu=signum(self.eta-epsilon)
            #Calculate W_prime by equation (11)
            W_prime=neuron.W-(neuron.W**self.alpha)*mu
            #Calculate the age of the post synaptic neuron by equation (12)
            neuron.W=W_prime*self.f0_ft
            neuron.W=max([neuron.W,0]) #The age can not be less than zero
            #print([beta_d,beta_l,epsilon])
    def neuronEvaluation_BT(self):
        weights=[]
        for neuronID in self.path:
            neuron=self.NeuronSet[neuronID]
            self.AgeStep(neuron)
            weights.append(self.calculateWeightFromTravelTime(neuron.T))
            #We calculate the weights from traveltimes. No exact formula was given, so I use a special function for this
        #print(weights)
        #print("Finish")
        for neuronID in self.path:
            neuron=self.NeuronSet[neuronID]
            #calculate p by (16)
            p=0
            if neuron.h>=self.theta:
                p=1
                #print(neuron.ID)
                neuron.t_f=self.ElapsedTime #Last time of firing becomes the current time
                if neuron.W<=0:
                    #If the age reaches zero, the synaptic pruning is performed
                    #Create new connection
                    n_before=self.NeuronSet[self.path[self.path.index(neuronID)-1]]
                    n_after=self.NeuronSet[self.path[self.path.index(neuronID)+1]]
                    try:
                        index=n_before.C_i.index(n_after.ID)
                    except:
                        n_before.C_i.append(n_after.ID)
                        n_before.C_i[0]+=1
                    try:
                        index=n_after.C_i.index(n_before.ID)
                    except:
                        n_after.C_i.append(n_before.ID)
                        n_after.C_i[0]+=1
                    #Delete the old connection
                    n_after.C_i.remove(neuron.ID)
                    n_after.C_i[0]-=1
                    n_before.C_i.remove(neuron.ID)
                    n_before.C_i[0]-=1
                    neuron.C_i.remove(n_after.ID)
                    neuron.C_i.remove(n_before.ID)
                    neuron.C_i[0]-=2
                    n_before.T+=neuron.T
                    self.path.remove(neuron.ID)
                    n_before.presynaptic=n_after.ID
            #calculate h_PSP by (20)
            neuron.h_PSP=math.exp((-1)*(self.ElapsedTime-neuron.t_f)/neuron.tau_BT)
            #calculate h_ref by (19)
            neuron.h_ref=self.gamma_ref*neuron.h_ref-p*self.R
            #calculate h_syn by (18)
            signalStrength=0
            potential=0
            #The signalStrength is the member with the summa. We add the weights multiplied with PSP-s of previous and next pathpoints. These are all elements added
            if p==0 or neuron.W>0:
                currentIndex=self.path.index(neuronID)
                potential=self.NeuronSet[self.path[currentIndex-1]].h_PSP
                try:
                    signalStrength+=weights[currentIndex]*self.NeuronSet[path[currentIndex+1]].h_PSP
                except:
                    pass
                if currentIndex>0:
                    signalStrength+=weights[currentIndex-1]*self.NeuronSet[self.path[currentIndex-1]].h_PSP
            #neuron.h_syn=self.gamma_syn*neuron.h_PSP+(sum(self.X[neuron.ID])-self.X[neuron.ID][neuron.ID])*neuron.h_PSP #I am not sure if the weights are actually the values of X, but we will see about that
            neuron.h_syn=self.gamma_syn*potential+signalStrength #The first member is multiplied with the next neuron's PSP in the row
            neuron.h=math.tanh(neuron.h_syn+neuron.h_ref)
            """
            if neuron.h_PSP>0.001:
                print(neuron.h_PSP)
            """
            #print([neuron.h,neuron.h_syn,neuron.h_ref])
    def BackwardTransmission(self,epochs):
        goalpointNeuron=self.NeuronSet[self.goalpoint]
        threshold=self.percentualSynapseRemoval*(len(self.path)-2)/100
        #print(threshold)
        goalpointNeuron.h=self.theta+0.1 #At the start of BT the goalpoint neuron must fire first
        e=0
        while e<epochs and len(self.path)>=threshold:
            self.neuronEvaluation_BT()
            self.ElapsedTime+=1 #self.CycleTime
            e+=1

def getStraightWayPoints(startpoint,goalpoint,mapInfo):
    #Startpoint and goalpoint are numbers, that mark their exact place on the map
    #We imagine a coordinate system, where the startpoint is at (0.5,0.5) and the goal is at the same range as before
    #The goal is to find all pathsegments, that must be crossed by the straight line
    mapWidth=mapInfo.width
    horizontalDistance=(goalpoint%mapWidth)-(startpoint%mapWidth)
    verticalDistance=(goalpoint//mapWidth)-(startpoint//mapWidth)
    slope=math.atan2((verticalDistance),(horizontalDistance))
    intercept=0.5-math.tan(slope)*0.5


def goalDir(robot_pos,goal_pos):
    horizontalDistance=goal_pos[0]-robot_pos[0]
    verticalDistancegoal_pos[1]-robot_pos[1]
    distanceFromGoal=math.sqrt(horizontalDistance**2+verticalDistance**2)
    goalOrientation=math.degrees(math.atan2(verticalDistance,horizontalDistance))
    return [distanceFromGoal,goalOrientation]

def calculateGoalDirection(robotOdometry,mapPoint,mapInfo):
    robotOnMap=odomOnMap(robotOdometry,mapInfo)
    horizontalDistance=(mapPoint%mapInfo.width)-(robotOnMap%mapInfo.width)
    verticalDistance=(mapPoint//mapInfo.width)-(robotOnMap//mapInfo.width)
    distanceFromGoal=math.sqrt(horizontalDistance**2+verticalDistance**2)*mapInfo.resolution
    goalOrientation=math.degrees(math.atan2(verticalDistance,horizontalDistance))
    return [distanceFromGoal,goalOrientation]

def UnitStep(x):
    y=0
    if x>=0:
        y=1
    return y

def keepYourEyeOnTheGoal(robotOrientation,goalDirection,tolerance):
    turning_basic=0.2
    sign=0
    diff=goalDirection-robotOrientation
    print(goalDirection)
    print(robotOrientation)
    print(diff)
    if diff>0:
        sign=-1
    elif diff<0:
        sign=1
    if abs(diff)>tolerance:
        turning=turning_basic*sign*(-1)

        if abs(diff)>(tolerance*2):
            turning=turning*2

    else:
        turning=0
    return turning

PID_Error=0
PID_Errsum=0
PID_Errdiff=0
PID_PrevErr=0

def turning_PID(robotOrientation,goalDirection,reset):
    global PID_Error
    global PID_Errsum
    global PID_Errdiff
    global PID_PrevErr
    P=0.009
    I=0
    D=0.005
    diff=goalDirection-robotOrientation
    print(goalDirection)
    print(robotOrientation)
    print(diff)
    PID_Error=diff
    if reset:
        PID_Errsum=0
        PID_PrevErr=PID_Error
    PID_Errsum+=PID_Error
    PID_Errdiff=PID_Error-PID_PrevErr
    PID_PrevErr=PID_Error
    turning=PID_Error*P+PID_Errsum*I+PID_Errdiff*D
    return turning

    '''
    #Ezen a reszen a Dijksra fele utkereso algoritmus is ki van dolgozva, de nincs hasznalva, mert hianyzik belole a terkepcsokkentes, igy sajnos nagyon lassu. Csak azert nem toroltem ki, mert mindennek ellenere tul sok munkam van benne
class vertex():
    def __init__(self,placeOnTheMap,mapInfo):
        self.x=placeOnTheMap%mapInfo.width
        self.y=placeOnTheMap//mapInfo.width
        self.dist=float("inf")
        self.prev=None
        self.neighbors=[]
        self.info=mapInfo
        self.place=placeOnTheMap
        self.targetPosition=False
    def defineNeighbors(self,vertexGroup):
        if self.place%self.info.width>0:
            self.neighbors.append(vertexGroup[self.place-1])
        if self.place%self.info.width<(self.info.width-1):
            self.neighbors.append(vertexGroup[self.place+1])
        if self.place>=self.info.width:
            self.neighbors.append(vertexGroup[self.place-self.info.width])
        if self.place<(self.info.height*(self.info.width-1)):
            self.neighbors.append(vertexGroup[self.place+self.info.width])


def Dijkstras(mapData,odomStart,odomGoal):
    vertexSet=[]
    evaluatedSet=[]
    for x in range(mapData.info.width*mapData.info.height):
        vertexSet.append(vertex(x,mapData.info))
    for y in vertexSet:
        y.defineNeighbors(vertexSet)
    vertexSet[int(odomOnMap(odomStart,mapData.info))].dist=0 #kiindulo helyzet
    vertexSet[int(odomOnMap(odomGoal,mapData.info))].targetPosition=True
    start=vertexSet[int(odomOnMap(odomStart,mapData.info))]
    while len(vertexSet)>0:
        lowestIndex=0
        for x in range(len(vertexSet)):
            if vertexSet[lowestIndex].dist>vertexSet[x].dist:
                lowestIndex=x
        current=vertexSet[lowestIndex]
        vertexSet.pop(lowestIndex)
        for x in current.neighbors:
            if 1!=abs(current.place-x.place) and abs(current.place-x.place)!=mapData.info.width:
                alt=current.dist+math.sqrt(2)
                #print("Itt vagyok")
            else:
                alt=current.dist+1
            if alt<x.dist:
                x.dist=alt
                x.prev=current
        evaluatedSet.append(current)
    print("Evaluation complete!")
    sequence=[]
    u=None
    for x in evaluatedSet:
        if x.targetPosition:
            u=x
    if u.prev!=None or u==start:
        while u!=None:
            sequence.append(u.place)
            u=u.prev
    sequence.reverse()
    return sequence

'''


"""
mapPoints=[0]*400
for i in range(0,20):
    mapPoints[200+i]=100
mapPoints[217]=0
'''
mapPoints=randomMap(20,20,False,10)
mapPoints[3][4]=0
mapPoints[3][16]=0
'''
#print(mapPoints)

example=NeuroActivityBasedDynamicPathPlanner(5,mapPoints,20)
example.startpoint=[3,4]
example.goalpoint=[3,16]
example.ForwardTransmission(2000)
'''
for s in example.SignalSet:
    print([s.source.ID,s.target.ID])
for point in example.path:
    print(example.NeuronSet[point].T)
'''
example.BackwardTransmission(1000)

for point in example.path:
    print(example.NeuronSet[point].W)

print(example.path)
FuzzyRulebook.writeSpreadsheetDataFile([example.mapData],"Map.txt")
FuzzyRulebook.writeSpreadsheetDataFile([example.path],"Path.txt")

#print(randomMap(20,20,False,10))
"""