#! /usr/bin/env python

import rospy
import math
import os
import FuzzyRulebook
import PathPlanner
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
from nav_msgs.msg import OccupancyGrid

def lidarCallback(msg):
    global laserDataStore
    laserDataStore=msg
def odomCallback(msg):
    global robotPlace
    robotPlace=msg
def mapCallback(msg):
    global drawnMap
    drawnMap=msg

rospy.init_node('BurgerController')
laserDataStore=LaserScan()
robotPlace=Odometry()
goalPosition=Odometry()
drawnMap=OccupancyGrid()
rate=rospy.Rate(2)
stateIndicator="initialize"
cycleCounter=0
pub=rospy.Publisher("/cmd_vel", Twist, queue_size=1)
motorDataStore=Twist()
rospy.Subscriber("/odom", Odometry, odomCallback)
rate.sleep()
rospy.Subscriber("/map", OccupancyGrid, mapCallback)
rate.sleep()
"""

gmapId=PathPlanner.getSlamId()
PathPlanner.pauseMapping(gmapId)
input_x=raw_input("x to reach:")
PathPlanner.continueMapping(gmapId)

"""




while stateIndicator!="end":
    if stateIndicator=="initialize":
        print("Preporation for the task...")
        fuzzyControlTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionTable.txt")
        safetyRule=FuzzyRulebook.fuzzyruleFromFile("SafetySet.txt")
        speedRule=FuzzyRulebook.fuzzyruleFromFile("MotorSpeedSet.txt")
        decision=0
        motorSpeed=0
        turn=0
        firstRound=False
        stateIndicator="inputGoal"
    elif stateIndicator=="inputGoal":
        print("You may give the required position for the robot. First the x, then the y coordinates")
        input_x=raw_input("x to reach:")
        input_y=raw_input("y to reach:")
        goalPosition.pose.pose.position.x=float(input_x)
        goalPosition.pose.pose.position.y=float(input_y)
        stateIndicator="pathplanning"
    elif stateIndicator=="pathplanning":
        rospy.Subscriber("/odom", Odometry, odomCallback)
        rate.sleep()
        rospy.Subscriber("/map", OccupancyGrid, mapCallback)
        rate.sleep()
        pathpoints=PathPlanner.A_star(drawnMap,robotPlace,goalPosition,8,firstRound)
        '''
        PathPlanner.reduceMapResolution(drawnMap,8)
        brain=PathPlanner.NeuroActivityBasedDynamicPathPlanner(1,drawnMap.data,drawnMap.info.width)
        brain.startpoint=[int((robotPlace.pose.pose.position.x/drawnMap.info.resolution)+(drawnMap.info.width/2)),int((robotPlace.pose.pose.position.y/drawnMap.info.resolution)+(drawnMap.info.width/2))] #El kell tolnunk a koordinatarendszert hogy jo legyen az algoritmusunknak
        brain.goalpoint=[int((goalPosition.pose.pose.position.x/drawnMap.info.resolution)+(drawnMap.info.width/2)),int((goalPosition.pose.pose.position.y/drawnMap.info.resolution)+(drawnMap.info.width/2))]
        brain.ForwardTransmission(2000)
        brain.BackwardTransmission(1000)
        pathpoints=brain.path
        pathpoints=PathPlanner.transformFoundPathToTheOriginalMap(drawnMap.info,8,pathpoints)
        pathpoints=list(reversed(pathpoints))
        print(pathpoints)
        '''
        firstRound=False
        FuzzyRulebook.pathlog(pathpoints)
        if len(pathpoints)>0:
            pathpoints.pop(0)
        stateIndicator="aquisition"
    elif stateIndicator=="setupDestination":
        if len(pathpoints)>0:
            rospy.Subscriber("/map", OccupancyGrid, mapCallback)
            rate.sleep()
            #goalSight=PathPlanner.calculateGoalDirection(robotPlace,pathpoints[len(pathpoints)-1],drawnMap.info)
            goalSight=PathPlanner.calculateGoalDirection(robotPlace,pathpoints[0],drawnMap.info)
            print(pathpoints)
            print(goalSight)
            if len(pathpoints)>1:
                goalTolerance=1
                goalAngleTolerance=30
            else:
                goalTolerance=0.2
                goalAngleTolerance=45
            if goalSight[0]<=goalTolerance:
                #Ez azt jelenti, hogy kozel van a celpozicio. Ekkor kivesszuk az utitervbol, es ujraszamoljuk a celt
                pathpoints.pop(0)
                stateIndicator="setupDestination"
            else:
                stateIndicator="motion"
        else:
            print("Target reached!")
            motorDataStore.linear.x=0
            motorDataStore.angular.z=0
            pub.publish(motorDataStore)
            rate.sleep()
            answer=raw_input("Would you like to give another (y/n)?")
            if answer=="y":
                stateIndicator="inputGoal"
            else:
                stateIndicator="end"
    elif stateIndicator=="aquisition":
        '''
        rospy.Subscriber("/scan", LaserScan, lidarCallback)
        rate.sleep()
        safety=FuzzyRulebook.fuzzyfyLidarData(laserDataStore,[[0,90],[270,360]])
        decision=FuzzyRulebook.controlAction(fuzzyControlTable,safetyRule,safety,speedRule,motorSpeed)
        '''
        rospy.Subscriber("/odom", Odometry, odomCallback)
        rate.sleep()
        print(robotPlace.pose.pose.position.x,robotPlace.pose.pose.position.y)
        stateIndicator="setupDestination"
    elif stateIndicator=="motion":
        motorSpeed=motorSpeed+decision
        motorSpeed=0.15
        #print(motorSpeed)
        robotOrientation=PathPlanner.quaternion_to_euler(robotPlace.pose.pose.orientation.x,robotPlace.pose.pose.orientation.y,robotPlace.pose.pose.orientation.z,robotPlace.pose.pose.orientation.w)
        turn=PathPlanner.keepYourEyeOnTheGoal(robotOrientation[2],goalSight[1],goalAngleTolerance)
        '''
        if motorSpeed>0:
            motorDataStore.linear.x=motorSpeed
            #turn=PathPlanner.keepYourEyeOnTheGoal(robotOrientation[2],goalSight[1],5)
            motorDataStore.angular.z=turn
        else:
            motorSpeed=0
            motorDataStore.linear.x=motorSpeed
            motorDataStore.angular.z=turn
        '''
        if turn!=0:
            motorDataStore.linear.x=0
            motorDataStore.angular.z=turn
        else:
            motorDataStore.linear.x=motorSpeed
            motorDataStore.angular.z=0
        #motorDataStore.linear.x=motorSpeed
        pub.publish(motorDataStore)
        stateIndicator="looptime"
    elif stateIndicator=="looptime":
        cycleCounter+=1
        if turn==0 and motorSpeed==0:
            stateIndicator="pathplanning"
            print("This seems to be too risky! Replanning...")
        else:
            stateIndicator="aquisition"
        if cycleCounter>=40:
            stateIndicator="end"
        print(cycleCounter)
        rate.sleep()
    else:
        print("ERROR: Unknown state!")
        stateIndicator="end"


motorDataStore.linear.x=0
motorDataStore.angular.z=0
pub.publish(motorDataStore)
#Itt a vege
#Meg esetleg a terkepet elmenthetjuk
f=os.popen("rostopic echo map -n1")
maplog=open("/home/user/simulation_ws/src/homework/src/Map.txt",'w')
maplog.write(f.read())
maplog.close()
