#! /usr/bin/env python

import math

class fuzzySet():
    def  __init__(self, nameOfSet, suppMin, coreMin, coreMax, suppMax):
        self.setName=nameOfSet
        self.suppMinimum=float(suppMin)
        self.suppMaximum=float(suppMax)
        self.coreMaximum=float(coreMax)
        self.coreMinimum=float(coreMin)
    def membership(self, element):
        if self.coreMinimum<=element<=self.coreMaximum:
            return 1
        elif element<self.suppMinimum or element>self.suppMaximum:
            return 0
        elif self.coreMaximum<element and element<=self.suppMaximum:
            slope=(0-1)/(self.suppMaximum-self.coreMaximum)
            b=(0-slope*self.suppMaximum)
            membershipValue=element*slope+b
            return membershipValue
        elif self.suppMinimum<=element and element<self.coreMinimum:
            slope=(1-0)/(self.coreMinimum-self.suppMinimum)
            b=(0-slope*self.suppMinimum)
            membershipValue=element*slope+b
            return membershipValue
        else:
            print("Fuzzy control error within the fuzzySet class!")
    def area(self,height):
        areaHeight=min([max([0,height]),1])
        setArea=(((self.suppMaximum-(self.suppMaximum-self.coreMaximum)*areaHeight)-(self.suppMinimum+(self.coreMinimum-self.suppMinimum)*areaHeight))+(self.suppMaximum-self.suppMinimum))*areaHeight/2
        return setArea
    def coreCenter(self,height):
        areaHeight=min([max([0,height]),1])
        center=((self.suppMaximum-(self.suppMaximum-self.coreMaximum)*areaHeight)+(self.suppMinimum+(self.coreMinimum-self.suppMinimum)*areaHeight))/2
        #center=(self.coreMaximum+self.coreMinimum)/2
        return center
    def centroid(self,height):
        coreMinMod=self.suppMinimum+height*(self.coreMinimum-self.suppMinimum)
        coreMaxMod=self.suppMaximum-height*(self.suppMaximum-self.coreMaximum)
        left=(coreMinMod-self.suppMinimum)*height/2
        middle=(coreMaxMod-coreMinMod)*height
        right=(self.suppMaximum-coreMaxMod)*height/2
        AreaHalf=self.area(height)/2
        if (AreaHalf>left) and (AreaHalf>right):
            center=((coreMaxMod+coreMinMod)/2+(self.suppMaximum+self.suppMinimum)/2)/2
        elif AreaHalf<left:
            center=math.sqrt(AreaHalf/left)*(coreMinMod-self.suppMinimum)+self.suppMinimum
        elif AreaHalf<right:
            center=self.suppMaximum-math.sqrt(AreaHalf/right)*(self.suppMaximum-coreMaxMod)
        else:
            center=self.coreCenter(height)
            print("Impossible to calculate the centroid!")
        return center
    def slopes(self):
        #Visszaadja a meredekseg abszolut erteket mindket oldalon
        slope_1=abs(1/(self.coreMinimum-self.suppMinimum))
        slope_2=abs(-1/(self.suppMaximum-self.coreMaximum))
        return [slope_1,slope_2]

def writeSpreadsheetDataFile(dataToWrite, fileName):
    textToSave=""
    for x in dataToWrite:
        for y in x:
            textToSave=textToSave+str(y)+"\t"
        textToSave=textToSave[:-1]
        textToSave=textToSave+"\n"
    textToSave=textToSave[:-1] #utolso karaktert kivesszuk
    writeData=open(fileName,'w')
    writeData.write(textToSave)
    writeData.close()

def readSpreadsheetDataFile(fileName):
    ssData=[]
    fileReader=open(fileName,'r')
    allRows=fileReader.read()
    rowsList=allRows.split('\n')
    for x in rowsList:
        if x!='':
            cellData=x.split('\t')
            ssData.append(cellData)
    return ssData

def getLidarSettings(laserdata):
    #csak a LaserScan tipusu adat jo ide
    resolution=len(laserdata.ranges)
    min_ang=laserdata.angle_min
    max_ang=laserdata.angle_max
    ang_step=laserdata.angle_increment
    offset=0-min_ang
    factor=round((max_ang-min_ang)/(2*math.pi),3)
    return (factor,offset)

def fuzzyfyLidarData(laserdata,domains):
    #osszeadjuk a mert tavolsagokat. Minel tobb dolog van a kozelben, annal veszelyesebb a kornyek. Ebben annyi hiba van, hogy keves a visszajelzes arrol, hogy milyen kozel van a targy
    settings=getLidarSettings(laserdata)
    amountOfSafety=0
    for x in domains:
        #a domains elso eleme a minimum, masodik a maximum fokban megadva
        min_rad=math.radians(x[0])
        max_rad=math.radians(x[1])
        minArrEl=round((min_rad+settings[1])/laserdata.angle_increment,0)
        maxArrEl=round((max_rad+settings[1])/laserdata.angle_increment,0)
        for y in range(int(minArrEl),int(maxArrEl)):
            if laserdata.ranges[y]<=laserdata.range_max:
                amountOfSafety=amountOfSafety+(math.cos(math.radians(y))/laserdata.ranges[y])
                #azert a tavolsag reciprokat adjuk hozza, mert ahely veszelyesebb, ha kozelebb vannak a targyak
    print(amountOfSafety)
    return min(amountOfSafety,99999) #lekorlatozzuk a kimenetet

def fuzzyruleFromFile(fileName):
    table=readSpreadsheetDataFile(fileName)
    rule=[]
    for x in table:
        sampleSet=fuzzySet(x[0],float(x[1]),float(x[2]),float(x[3]),float(x[4]))
        rule.append(sampleSet)
    return rule    

def membershipArray(fuzzyrule,fuzzyVariable):
    #Ez meg is adja a tagsagot az osszes fuzzy halmazban
    membershipVector=[]
    for x in fuzzyrule:
        membershipVector.append(x.membership(fuzzyVariable))
    return membershipVector


def controlAction(actionTable,fuzzyRule1,fuzzyVariable1,fuzzyRule2,fuzzyVariable2):
    #A fuzzy szabalyok fuzzy halmazokbol allo tombot takarnak. Itt csak ez az adattipus mukodik
    #Celszeruen a mostani projektnel 1-es valtozo a veszelyesseg, 2-es a sebesseg
    #Kesobb is a szabalyozando kene hogy legyen a 2-es
    #Ebben a Center Of Gravity (COG) nevu defuzzyfikacios eljarast hasznalom
    memship1=membershipArray(fuzzyRule1,fuzzyVariable1)
    memship2=membershipArray(fuzzyRule2,fuzzyVariable2)
    num=0
    den=0
    i=0
    for x in actionTable:
        j=0
        for y in x:
            num=num+(min(memship1[j],memship2[i])*float(actionTable[i][j]))
            den=den+min(memship1[j],memship2[i])
            j=j+1
        i=i+1
    '''
    if den==0:
        print([fuzzyVariable1,fuzzyVariable2])
    '''
    return num/den

def pathlog(pathpoints):
    stringpoints=[]
    for x in pathpoints:
        stringpoints.append(str(x))
    #print([stringpoints])
    pathlog=open("/home/user/simulation_ws/src/homework/src/Path.txt",'w')
    writeSpreadsheetDataFile([stringpoints],"Path.txt")
    pathlog.close()

def controlAction_V2(antecedents1,fuzzyVariable1,antecedents2,fuzzyVariable2,consequents):
    memship1=membershipArray(antecedents1,fuzzyVariable1)
    memship2=membershipArray(antecedents2,fuzzyVariable2)
    num=0
    den=0
    for i in range(0,len(consequents)):
        w=min([memship1[i],memship2[i]])
        Area=consequents[i].area(w)
        den+=Area
        num+=Area*(consequents[i].coreCenter(w))
    try:
        decision=num/den
    except:
        decision=0
    return decision

def controlAction_V3(antecedents1,fuzzyVariable1,antecedents2,fuzzyVariable2,consequents,ruleTable):
    memship1=membershipArray(antecedents1,fuzzyVariable1)
    memship2=membershipArray(antecedents2,fuzzyVariable2)
    num=0
    den=0
    for x in range(0,len(ruleTable)):
        for y in range(0,len(ruleTable[x])):
            if int(ruleTable[x][y])>=0: #Ha a cella erteke -1, akkor az az eset nem ertelmezett
                w=min([memship1[y],memship2[x]])
                cons=consequents[int(ruleTable[x][y])]
                Area=cons.area(w)
                den+=Area
                num+=Area*(cons.coreCenter(w))
                #num+=Area*(cons.gravityCenter())
    try:
        decision=num/den
    except:
        decision=0
    return decision

def lineIntegral(startPoint,endPoint):
    #startPoint a tartomany kezdete, endPoint pedig a vege. Mindketto ketelemu tomb kell hogy legyen
    integral=0
    if startPoint[1]<0 or endPoint[1]<0:
        print("ERROR! This function is not ready to evaluate integrals with negative y values!")
    else:
        integral=(startPoint[1]+endPoint[1])*(endPoint[0]-startPoint[0])/2
    return integral

class VagueEnvironment():
    def __init__(self,fuzzyrule):
        self.sourceRule=fuzzyrule
        self.lines=[]
        if self.CheckValidity():
            self.sortByCoreCenter() #Ha megtortenik az atrendezes, a fuzzyrule argumentum is atrendezodik, vagyis annak a forrasa is
            slopes=[]
            for x in self.sourceRule:
                slopes.append(x.slopes())
            for j in range(0,len(self.sourceRule)):
                # Feltoltjuk a vonalak tombjet. A vonalak 2D tombok, ket pontbol allnak, egy x es egy y koordinatat tartalmaznak
                #Ha egy vonalnak csak egy pontja ismert (mert mondjuk vegtelen a masik iranyban), a masik pont helyen ures tomb van
                if j==0:
                    self.lines.append([[],[self.sourceRule[j].coreCenter(1),slopes[j][0]]])
                    #self.lines.append([[self.sourceRule[j].suppMinimum,slopes[j][0]],[self.sourceRule[j].coreCenter(1),slopes[j][0]]])
                else:
                    self.lines.append([[self.sourceRule[j-1].coreCenter(1),slopes[j-1][1]],[self.sourceRule[j].coreCenter(1),slopes[j][0]]])
                if j==(len(self.sourceRule)-1):
                    self.lines.append([[self.sourceRule[j].coreCenter(1),slopes[j][1]],[]])
                    #self.lines.append([[self.sourceRule[j].coreCenter(1),slopes[j][1]],[self.sourceRule[j].suppMaximum,slopes[j][1]]])
    def CheckValidity(self):
        #Itt a self.sourceRule csak egy csoport fuzzy halmazt jelent. Egy csoport antecedenst vagy konzekvenst, nem egy teljes szabalyt
        isValid=True
        for x in self.sourceRule:
            if x.coreMinimum!=x.coreMaximum:
                print("Invalid fuzzy membership function! They all must be triangle shaped!")
                isValid=False
        return isValid
    def sortByCoreCenter(self):
        numberOfReplacements=10
        temp=None
        while numberOfReplacements!=0:
            numberOfReplacements=0
            for j in range(0,len(self.sourceRule)-1):
                if self.sourceRule[j].coreCenter(1)>self.sourceRule[j+1].coreCenter(1):
                    temp=self.sourceRule[j]
                    self.sourceRule[j]=self.sourceRule[j+1]
                    self.sourceRule[j+1]=temp
                    numberOfReplacements+=1
        numberOfCombinations=10
        while numberOfCombinations!=0:
            numberOfCombinations=0
            index=0
            #Ha vannak olyanok, amelyeknek kozos a magpontjuk, kivesszuk azokat, es atlagoljuk a meredekseguket
            #Ekkor a forras is megvaltozik, de a teljesen eredetire szerintem nem lesz szukseg
            while index<(len(self.sourceRule)-1):
                if self.sourceRule[index].coreCenter(1)==self.sourceRule[index+1].coreCenter(1):
                    self.sourceRule[index].suppMinimum=(self.sourceRule[index].suppMinimum+self.sourceRule[index+1].suppMinimum)/2
                    self.sourceRule[index].suppMaximum=(self.sourceRule[index].suppMaximum+self.sourceRule[index+1].suppMaximum)/2
                    self.sourceRule.pop(index+1)
                    numberOfCombinations+=1
                else:
                    index+=1
    def getValueOnLine(self,x,lineIndex):
        i=lineIndex
        value=0
        try:
            value=(x-self.lines[i][0][0])/(self.lines[i][1][0]-self.lines[i][0][0])*(self.lines[i][1][1]-self.lines[i][0][1])+self.lines[i][0][1]
        except:
            try:
                value=self.lines[i][0][1]
            except:
                value=self.lines[i][1][1]
        return value
    def VagueDistance(self,point1,point2):
        #Eleg a pontok helyet megadni
        lineIndexes=[]
        #A ket lineIndex megadja, hogy hanyadik vonalon van rajta az adott pont
        points=[point1,point2]
        points=sorted(points)
        for i in points:
            for j in range(0,len(self.lines)):
                if self.lines[j][0]==[] and self.lines[j][1]==[]:
                    print("ERROR! Invalid line!")
                if self.lines[j][0]==[] and i<self.lines[j][1][0]:
                    lineIndexes.append(j)
                if self.lines[j][1]==[] and i>=self.lines[j][0][0]:
                    lineIndexes.append(j)
                if self.lines[j][0]!=[] and self.lines[j][1]!=[]:
                    if i<self.lines[j][1][0] and i>=self.lines[j][0][0]:
                        lineIndexes.append(j)
        if len(lineIndexes)!=2:
            print("ERROR! Incorrect point placement!")
            print(lineIndexes)
            print(self.lines)
        #lineIndexes=sorted(lineIndexes)
        values=[] #A pontok y ertekei
        for i in range(0,len(points)):
            values.append(self.getValueOnLine(points[i],lineIndexes[i]))
        delta=0
        if lineIndexes[0]==lineIndexes[1]:
            delta=lineIntegral([points[0],values[0]],[points[1],values[1]])
        else:
            for i in range(lineIndexes[0],lineIndexes[1]+1):
                if i==lineIndexes[0]:
                    delta+=lineIntegral([points[0],values[0]],self.lines[i][1])
                elif i==lineIndexes[1]:
                    delta+=lineIntegral(self.lines[i][0],[points[1],values[1]])
                else:
                    delta+=lineIntegral(self.lines[i][0],self.lines[i][1])
        for i in range(0,len(points)):
            if points[i]<=self.sourceRule[0].suppMinimum:
                delta-=lineIntegral([points[i],self.lines[0][1][1]],[self.sourceRule[0].suppMinimum,self.lines[0][1][1]])
            if points[i]>=self.sourceRule[len(self.sourceRule)-1].suppMaximum:
                delta-=lineIntegral([self.sourceRule[len(self.sourceRule)-1].suppMaximum,self.lines[len(self.lines)-1][0][1]],[points[i],self.lines[len(self.lines)-1][0][1]])
        if lineIndexes[0]==lineIndexes[1]:
            if points[0]<=self.sourceRule[0].suppMinimum and points[1]<=self.sourceRule[0].suppMinimum:
                delta=0
            if points[0]>=self.sourceRule[len(self.sourceRule)-1].suppMaximum and points[1]>=self.sourceRule[len(self.sourceRule)-1].suppMaximum:
                delta=0
        return delta

def solveQuadraticEquation(a,b,c):
    solutions=[]
    D=b**2-4*a*c
    if D>=0:
        s1=((-1*b)+math.sqrt(D))/(2*a)
        s2=((-1*b)-math.sqrt(D))/(2*a)
        solutions=[s1,s2]
    else:
        print("ERROR! This function is not ready to handle complex solutions!")
        solutions=[0,0]
    return solutions

def controlAction_FIVE(antecedents1,fuzzyVariable1,antecedents2,fuzzyVariable2,consequents,ruleTable,y0,p=1):
    num=0
    den=0
    env_1=VagueEnvironment(antecedents1)
    env_2=VagueEnvironment(antecedents2)
    env_cons=VagueEnvironment(consequents)
    for i in range(0,len(ruleTable)):
        for j in range(0,len(ruleTable[i])):
            if int(ruleTable[i][j])>=0: #Ha a cella erteke -1, akkor az az eset nem ertelmezett
                #Nem mindegy, melyik i es j. A tablazat sora, vagy oszlopa
                w=math.sqrt(env_1.VagueDistance(fuzzyVariable1,antecedents1[j].coreCenter(1))**2+env_2.VagueDistance(fuzzyVariable2,antecedents2[i].coreCenter(1))**2)
                w=1/((w)**p)
                #print([w,antecedents1[j].coreCenter(1)])
                den+=w
                delta_by0=env_cons.VagueDistance(consequents[int(ruleTable[i][j])].coreCenter(1),y0)
                num+=delta_by0*w
                #print([num,den,delta_by0])
    delta_sy=num/den
    y=None
    env_cons.lines[0][0]=[max([y0,env_cons.sourceRule[0].suppMinimum]),env_cons.lines[0][1][1]]
    for i in range(0,len(env_cons.lines)):
        if delta_sy>0:
            if i!=(len(env_cons.lines)-1):
                diff=env_cons.VagueDistance(env_cons.lines[i][0][0],env_cons.lines[i][1][0])
            else:
                diff=delta_sy
            if abs(delta_sy-diff)<0.000001:
                diff=delta_sy # Nagyon kis kulonbseg eseten is 0 a kulonbseg
            if delta_sy<=diff:
                slope=0
                if i!=(len(env_cons.lines)-1):
                    slope=(env_cons.lines[i][1][1]-env_cons.lines[i][0][1])/(env_cons.lines[i][1][0]-env_cons.lines[i][0][0])
                    if abs(slope)<0.000001:
                        slope=0 #Nagyon kis meredekseg eseten is 0 a meredekseg
                if slope!=0:
                    #y=(delta_sy-env_cons.lines[i][0][1])/(slope/2)+env_cons.lines[i][0][0]
                    y=0
                    possibleSolutions=solveQuadraticEquation((slope/2),env_cons.lines[i][0][1],(-1*delta_sy))
                    if slope>0:
                        y=max(possibleSolutions) # A kisebb megoldas negativ. Ellenkezo iranyu integralast jelentene
                    else:
                        y=min(possibleSolutions) # A nagyobb megoldas az az eset, mikor az egyenes tulmegy x tengelyen. Ez hamis megoldas lenne
                    y+=env_cons.lines[i][0][0]
                    #Ez adja y-t eredmenyul. Ennek oka egy bonyolult integralos magia miatt van (valojaban nem bonyolult, csak mar nincs olyan agykapacitasom, hogy egyszeruen ertsem)
                else:
                    y=delta_sy/env_cons.lines[i][0][1]+env_cons.lines[i][0][0]
            delta_sy-=diff
            #print(delta_sy)
    return y


'''
exam1=[fuzzySet("R1_1",1,2,2,3),fuzzySet("R1_2",2,4,4,6)]
exam2=[fuzzySet("R2_1",1,3,3,5),fuzzySet("R2_2",1,2,2,3)]
cons=[fuzzySet("C1",3,4,4,5),fuzzySet("C2",4,5.5,5.5,7)]
result=controlAction_V2(exam1,2.6,exam2,2.4,cons)
print(result)
'''

'''
exam=fuzzySet("Pelda",1,2,3,22)
print(exam.centroid(0.9))
print(exam.slopes())
'''

'''
rule=fuzzyruleFromFile("SafetySet_Test.txt")
env=VagueEnvironment(rule)
val=env.VagueDistance(12,30)
print(val)
'''
'''
safety=fuzzyruleFromFile("SafetySet_Test.txt")
speed=fuzzyruleFromFile("MotorSpeedSet_Test.txt")
action=fuzzyruleFromFile("ActionSet_Test.txt")
table=readSpreadsheetDataFile("ControlActionLookupTable_Test.txt")

env=VagueEnvironment(safety)
env2=VagueEnvironment(speed)
env3=VagueEnvironment(action)
print(env.sourceRule[2].coreCenter(1))
print(env2.sourceRule[0].coreCenter(1))
print(env3.sourceRule[3].coreCenter(1))
#print(env3.lines)
print(env.VagueDistance(15,env.sourceRule[2].coreCenter(1)))
print(env2.VagueDistance(0.5,env2.sourceRule[0].coreCenter(1)))
print(env3.VagueDistance(-1,env3.sourceRule[3].coreCenter(1)))

print(env.VagueDistance(15,env.sourceRule[3].coreCenter(1)))
print(env2.VagueDistance(0.5,env2.sourceRule[2].coreCenter(1)))
print(env3.VagueDistance(-1,env3.sourceRule[1].coreCenter(1)))

Y=controlAction_FIVE(safety,15,speed,0.5,action,table,-1)
#Y=controlAction_FIVE([fuzzySet("Name",1,2,2,3),fuzzySet("Name",2,4,4,6)],2.6,[fuzzySet("Name",1,3,3,5),fuzzySet("Name",1,2,2,3)],2.4,[fuzzySet("Name",3,4,4,5),fuzzySet("Name",4,5.5,5.5,7)],[[0,-1],[-1,1]],-1)
print(Y)
'''