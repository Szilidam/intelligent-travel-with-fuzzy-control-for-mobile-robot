#! /usr/bin/env python

import rospy
import math
import os
import FuzzyRulebook
import PathPlanner
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
from nav_msgs.msg import OccupancyGrid

def lidarCallback(msg):
    global laserDataStore
    laserDataStore=msg
def odomCallback(msg):
    global robotPlace
    robotPlace=msg
def mapCallback(msg):
    global drawnMap
    drawnMap=msg


rospy.init_node('BurgerController')
laserDataStore=LaserScan()
robotPlace=Odometry()
goalPosition=Odometry()
drawnMap=OccupancyGrid()
currentGoal=[0,0]
rate=rospy.Rate(4)
stateIndicator="initialize"
cycleCounter=0
pub=rospy.Publisher("/cmd_vel", Twist, queue_size=1)
motorDataStore=Twist()
rospy.Subscriber("/odom", Odometry, odomCallback)
rate.sleep()
rospy.Subscriber("/map", OccupancyGrid, mapCallback)
rate.sleep()
while stateIndicator!="end":
    if stateIndicator=="initialize":
        #fuzzyControlTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionTable.txt")
        safetyRule=FuzzyRulebook.fuzzyruleFromFile("SafetySet.txt")
        speedRule=FuzzyRulebook.fuzzyruleFromFile("MotorSpeedSet.txt")
        decisionOptions=FuzzyRulebook.fuzzyruleFromFile("ActionSet.txt")
        lookupTable=FuzzyRulebook.readSpreadsheetDataFile("ControlActionLookupTable.txt")
        decision=0
        motorSpeed=0
        turn=0
        firstRound=False
        #stateIndicator="aquisition"
        stateIndicator="inputGoal"
    elif stateIndicator=="inputGoal":
        print("You may give the required position for the robot. First the x, then the y coordinate")
        input_x=raw_input("x to reach:")
        input_y=raw_input("y to reach:")
        #currentGoal[0]=float(input_x)
        #currentGoal[1]=float(input_y)
        goalPosition.pose.pose.position.x=float(input_x)
        goalPosition.pose.pose.position.y=float(input_y)
        stateIndicator="pathplanning"
        #stateIndicator="aquisition"
    elif stateIndicator=="pathplanning":
        #cycleCounter=0
        rospy.Subscriber("/odom", Odometry, odomCallback)
        rate.sleep()
        rospy.Subscriber("/map", OccupancyGrid, mapCallback)
        rate.sleep()
        pathpoints=PathPlanner.A_star(drawnMap,robotPlace,goalPosition,8,firstRound)
        '''
        PathPlanner.reduceMapResolution(drawnMap,8)
        brain=PathPlanner.NeuroActivityBasedDynamicPathPlanner(1,drawnMap.data,drawnMap.info.width)
        brain.startpoint=[int((robotPlace.pose.pose.position.x/drawnMap.info.resolution)+(drawnMap.info.width/2)),int((robotPlace.pose.pose.position.y/drawnMap.info.resolution)+(drawnMap.info.width/2))] #El kell tolnunk a koordinatarendszert hogy jo legyen az algoritmusunknak
        brain.goalpoint=[int((goalPosition.pose.pose.position.x/drawnMap.info.resolution)+(drawnMap.info.width/2)),int((goalPosition.pose.pose.position.y/drawnMap.info.resolution)+(drawnMap.info.width/2))]
        brain.ForwardTransmission(2000)
        #brain.BackwardTransmission(1000)
        pathpoints=brain.path
        pathpoints=PathPlanner.transformFoundPathToTheOriginalMap(drawnMap.info,8,pathpoints)
        pathpoints=list(reversed(pathpoints))
        print(pathpoints)
        '''
        firstRound=False
        FuzzyRulebook.pathlog(pathpoints)
        if len(pathpoints)>0:
            pathpoints.pop(0)
        rospy.Subscriber("/map", OccupancyGrid, mapCallback)
        rate.sleep()
        stateIndicator="aquisition"
    elif stateIndicator=="setupDestination":
        if len(pathpoints)>0:
            rospy.Subscriber("/map", OccupancyGrid, mapCallback)
            rate.sleep()
            #goalSight=PathPlanner.calculateGoalDirection(robotPlace,pathpoints[len(pathpoints)-1],drawnMap.info)
            goalSight=PathPlanner.calculateGoalDirection(robotPlace,pathpoints[0],drawnMap.info)
            print(pathpoints)
            print(goalSight)
            if len(pathpoints)>1:
                goalTolerance=0.5
                goalAngleTolerance=45
            else:
                goalTolerance=0.3
                goalAngleTolerance=45
            if goalSight[0]<=goalTolerance:
                #Ez azt jelenti, hogy kozel van a celpozicio. Ekkor kivesszuk az utitervbol, es ujraszamoljuk a celt
                pathpoints.pop(0)
                stateIndicator="setupDestination"
            else:
                stateIndicator="motion"
        else:
            print("Target reached!")
            motorDataStore.linear.x=0
            motorDataStore.angular.z=0
            pub.publish(motorDataStore)
            rate.sleep()
            answer=raw_input("Would you like to give another (y/n)?")
            if answer=="y":
                stateIndicator="inputGoal"
            else:
                stateIndicator="end"
    elif stateIndicator=="aquisition":
        rospy.Subscriber("/scan", LaserScan, lidarCallback)
        rate.sleep()
        rospy.Subscriber("/odom", Odometry, odomCallback)
        rate.sleep()
        safety=FuzzyRulebook.fuzzyfyLidarData(laserDataStore,[[0,90],[270,360]])
        decision=FuzzyRulebook.controlAction_FIVE(safetyRule,safety,speedRule,motorSpeed,decisionOptions,lookupTable,-1)
        #decision=FuzzyRulebook.controlAction(fuzzyControlTable,safetyRule,safety,speedRule,motorSpeed)
        print(robotPlace.pose.pose.position.x,robotPlace.pose.pose.position.y)
        stateIndicator="setupDestination"
        #stateIndicator="motion"
    elif stateIndicator=="motion":
        motorSpeed=motorSpeed+decision
        motorSpeed=min([motorSpeed,0.3])
        if len(pathpoints)==1:
            motorSpeed*=0.75
        #motorSpeed=0.15
        #print(motorSpeed)
        robotOrientation=PathPlanner.quaternion_to_euler(robotPlace.pose.pose.orientation.x,robotPlace.pose.pose.orientation.y,robotPlace.pose.pose.orientation.z,robotPlace.pose.pose.orientation.w)
        #turn=PathPlanner.keepYourEyeOnTheGoal(robotOrientation[2],goalSight[1],goalAngleTolerance)
        turn=PathPlanner.turning_PID(robotOrientation[2],goalSight[1],(cycleCounter==0))
        print(turn)
        if turn>=0.3:
            motorSpeed=0

        if motorSpeed>0:
            motorDataStore.linear.x=motorSpeed
            #turn=PathPlanner.keepYourEyeOnTheGoal(robotOrientation[2],goalSight[1],5)
            motorDataStore.angular.z=turn
        else:
            motorSpeed=0
            motorDataStore.linear.x=motorSpeed
            motorDataStore.angular.z=turn
        '''
        if turn!=0:
            motorDataStore.linear.x=0
            motorDataStore.angular.z=turn
            motorSpeed=0
        else:
            motorDataStore.linear.x=motorSpeed
            motorDataStore.angular.z=0
        '''
        #motorDataStore.linear.x=motorSpeed
        pub.publish(motorDataStore)
        stateIndicator="looptime"
    elif stateIndicator=="looptime":
        cycleCounter+=1
        stateIndicator="aquisition"
        if cycleCounter>=300:
            stateIndicator="end"
        print(cycleCounter)
        rate.sleep()
    else:
        print("ERROR: Unknown state!")
        stateIndicator="end"

motorDataStore.linear.x=0
motorDataStore.angular.z=0
pub.publish(motorDataStore)
#Itt a vege
#Meg esetleg a terkepet elmenthetjuk
f=os.popen("rostopic echo map -n1")
maplog=open("/home/user/simulation_ws/src/homework/src/Map.txt",'w')
maplog.write(f.read())
maplog.close()




